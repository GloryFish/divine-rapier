# Clean build directory
rm -rf build/*

# Export release info
git rev-parse --short HEAD > loveapp/release.txt

# Build .love
cd loveapp
zip -r ../build/divine-rapier.love *

cd ../build

# build Windows
cp -R ../love-windows ./divine-rapier-win
cat ./divine-rapier-win/love.exe ./divine-rapier.love > ./divine-rapier-win/divine-rapier.exe
rm ./divine-rapier-win/love.exe

# Build Mac

cp -R /Applications/love.app ../build/Divine\ Rapier.app
cp ./divine-rapier.love ./Divine\ Rapier.app/Contents/Resources/
/usr/libexec/PlistBuddy -c "Set :CFBundleIdentifier org.gloryfish.divine-rapier" ./Divine\ Rapier.app/Contents/Info.plist
/usr/libexec/PlistBuddy -c "Set :CFBundleName divine-rapier" ./Divine\ Rapier.app/Contents/Info.plist
/usr/libexec/PlistBuddy -c "Set :CFBundleSignature org.gloryfish.divine-rapier" ./Divine\ Rapier.app/Contents/Info.plist
/usr/libexec/PlistBuddy -c "Delete :UTExportedTypeDeclarations" ./Divine\ Rapier.app/Contents/Info.plist
/usr/libexec/PlistBuddy -c "Delete :CFBundleDocumentTypes" ./Divine\ Rapier.app/Contents/Info.plist

cp ../icon.icns ./Divine\ Rapier.app/Contents/Resources/OS\ X\ AppIcon.icns

echo "Build complete!"