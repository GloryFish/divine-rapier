--
--  map.lua
--

require 'middleclass'
require 'vector'
require 'colors'
require 'utility'
require 'waypoint'
require 'spatialhash'
require 'serpent'
require 'rectangle'
require 'astar'

local HC = require 'hardoncollider'

Map = class('Map')


function Map:initialize()
  self.background = love.graphics.newImage('resources/sprites/map.jpg')
  self.spatialhash = SpatialHash(100)
  self.waypoints = {}
  self.selectedWaypoint = {}

  self.mode = 'normal'

  Notifier:listenForMessage('world_click', self)

  self.astar = AStar(self)

  self:loadWaypointGraph()

  self.polygon_points = {}
  self.collider = HC(100, self.on_collision, self.collision_stop)
  self:loadWalls()
  self.selectedWalls = {}

  self.dire_spawn = vector(935, 112)
  self.radiant_spawn = vector(75, 905)

  Notifier:listenForMessage('area_effect', self)
  self.area_effects = {}
end

function Map:receiveMessage(message, data)
  if message == 'area_effect' then
    local stun = data
    table.insert(self.area_effects, stun)

  elseif message == 'world_click' then
    local position = data

    if self.mode == 'normal' then
      self.selectedWaypoint = nil

      -- Select first waypoint found here
      local nearbyWaypoints = self.spatialhash:objectsForPoint(position)
      for index, waypoint in ipairs(nearbyWaypoints) do
        if waypoint:contains(position) then
          self.selectedWaypoint = waypoint
          return
        end
      end

      -- Select walls found here
      self.selectedWalls =  self.collider:shapesAt(position.x, position.y)

    end

    if self.mode == 'add_waypoint' then
      self.selectedWaypoint = nil

      local waypoint = Waypoint(position)
      self.spatialhash:insertObjectForRect(waypoint, waypoint:getRect())
      self.waypoints[waypoint.id] = waypoint
      self.mode = 'normal'
    end

    if self.mode == 'move_waypoint' then
      -- Select first waypoint found here
      local waypoint = self.selectedWaypoint
      self.spatialhash:removeObjectForRect(waypoint, waypoint:getRect())
      waypoint.circle.position = position
      self.spatialhash:insertObjectForRect(waypoint, waypoint:getRect())
      self.mode = 'normal'
    end

    if self.mode == 'connect_waypoint' then
      -- Select first waypoint found here
      local nearbyWaypoints = self.spatialhash:objectsForPoint(position)
      for index, waypoint in ipairs(nearbyWaypoints) do
        if waypoint:contains(position) then
          self:connectWaypoints(self.selectedWaypoint, waypoint)
          self.mode = 'normal'
          return
        end
      end
    end

    if self.mode == 'disconnect_waypoint' then
      -- Select first waypoint found here
      local nearbyWaypoints = self.spatialhash:objectsForPoint(position)
      for index, waypoint in ipairs(nearbyWaypoints) do
        if waypoint:contains(position) then
          self:disconnectWaypoints(self.selectedWaypoint, waypoint)
          self.mode = 'normal'
          return
        end
      end
    end
  end
end

function Map:connectWaypoints(a, b)
  if a == b then
    return
  end

  a:connectTo(b.id)
  b:connectTo(a.id)
end

function Map:disconnectWaypoints(a, b)
  if a == b then
    return
  end

  a:disconnectFrom(b.id)
  b:disconnectFrom(a.id)
end

function Map:update(dt)
  if vars.editor then
    if love.keyboard.isDown('a') then
      self.mode = 'add_waypoint'
    end
    if love.keyboard.isDown('c') and self.selectedWaypoint ~= nil then
      self.mode = 'connect_waypoint'
    end
    if love.keyboard.isDown('v') and self.selectedWaypoint ~= nil then
      self.mode = 'disconnect_waypoint'
    end
    if love.keyboard.isDown('m') and self.selectedWaypoint ~= nil then
      self.mode = 'move_waypoint'
    end
    if love.keyboard.isDown('d') and self.selectedWaypoint ~= nil then
      local waypoint = self.selectedWaypoint
      if waypoint ~= nil then
        for id, _ in pairs(waypoint.connections) do
          local secondWaypoint = self.waypoints[id]
          self:disconnectWaypoints(waypoint, secondWaypoint)
        end

        self.spatialhash:removeObjectForRect(waypoint, waypoint:getRect())
        self.waypoints[waypoint.id] = nil
        self.mode = 'normal'
      end
    end

    if love.keyboard.isDown('d') and #self.selectedWalls > 0 then
      for index, wall in ipairs(self.selectedWalls) do
        self.collider:remove(wall)
        wall.deleteme = true
      end

      for i = #self.shapes, 1, -1 do
        if self.shapes[i].deleteme then
          table.remove(self.shapes, i)
        end
      end
      self.selectedWalls = {}
    end

    if love.keyboard.isDown('s') then
      self:saveWaypointGraph()
      self:saveWalls()
    end
  end

  self.collider:update(dt)

  for index, effect in ipairs(self.area_effects) do
    if love.timer.getTime() - effect.startTime > 0.05 then
      effect.deleteme = true
    end
  end

  for i = #self.area_effects, 1, -1 do
    if self.area_effects[i].deleteme then
      table.remove(self.area_effects, i)
    end
  end
end

function Map:draw()
  colors.white:set()
  love.graphics.draw(self.background, 0, 0, 0, 1, 1, 0, 0)
  -- Draw navigation graph
  if vars.showgraph then
    local viewport = self.camera:worldViewport()
    local waypoints = self.spatialhash:objectsForRect(viewport)

    for index, waypoint in pairs(waypoints) do
      local isSelected = waypoint == self.selectedWaypoint
      waypoint:draw(isSelected)

      for id, _ in pairs(waypoint.connections) do
        local secondWaypoint = self.waypoints[id]
        colors.blue:set()
        love.graphics.line(waypoint.circle.position.x, waypoint.circle.position.y,
                          secondWaypoint.circle.position.x, secondWaypoint.circle.position.y)
      end
    end
  end

  --draw spatial hash grid
  if vars.showhash then
    colors.red:set()
    for x = 1, 1280, self.spatialhash.cell_size do
      love.graphics.line(x, 0, x, love.graphics.getHeight())
    end

    for y = 1, 720, self.spatialhash.cell_size do
      love.graphics.line(0, y, love.graphics.getWidth(), y)
    end
  end

  -- draw walls
  if vars.showwalls then
    colors.orange:alpha(0.5 * 255):set()

    for index, shape in ipairs(self.shapes) do
      shape:draw('fill')
    end

        -- draw current polygon
    if #self.polygon_points >= 6 then
      colors.purple:set()
      love.graphics.polygon('line', self.polygon_points)
    else
      colors.purple:set()
      if self.polygon_points[1] and self.polygon_points[2] then
        love.graphics.circle('line', self.polygon_points[1], self.polygon_points[2], 3, 10)
      end

      if self.polygon_points[3] and self.polygon_points[4] then
        love.graphics.circle('line', self.polygon_points[3], self.polygon_points[4], 3, 10)
      end
    end

    -- draw selected walls
    for index, wall in ipairs(self.selectedWalls) do
      colors.yellow:alpha(0.5 * 255):set()
      wall:draw('fill')
    end
  end

end

function Map:drawOccluders()
  -- draw selected walls
  colors.black:set()
  for index, shape in ipairs(self.shapes) do
    shape:draw('fill')
  end
end

function Map:pointIsWalkable(point)
  for shape in pairs(self.collider:shapesAt(point:unpack())) do
    return false
  end
  return true
end

function Map:pathBetween(start_point, end_point, range)
  -- get start and end waypoints
  local start_waypoint = self:visibleWaypointInRange(start_point, range, end_point)
  if not start_waypoint == nil then
    print('no visible waypoint, getting any nearest')
    start_waypoint = self:nearestWaypointInRange(start_point, range, end_point)
  end

  local end_waypoint = self:visibleWaypointInRange(end_point, range)

  if not start_waypoint or not end_waypoint then
    return nil
  end

  -- Can we just walk in a straight line?
  local lookAt = end_point - start_point
  if not self.collider:intersectsRay(start_point.x, start_point.y, lookAt.x, lookAt.y) then
    return {end_point}
  end

  -- get path between waypoints
  local astar_path = self.astar:findPath(start_waypoint, end_waypoint)
  if astar_path == nil then
    print('astar sucked out')
    return nil
  else
    local path = {}
    table.insert(path, start_point)

    for index, node in ipairs(astar_path.nodes) do
      local waypoint = node.location
      table.insert(path, waypoint.circle.position)
    end

    table.insert(path, end_point)

    return path
  end
end

function Map:intersectsRay(origin, delta)
  return self.collider:intersectsRay(origin.x, origin.y, delta.x, delta.y)
end

-- Finds a waypoint near 'point' within 'range', ensures that there is direct line of sight to the waypoint
function Map:visibleWaypointInRange(point, range, target)
  local offset = vector(range, range)
  local rect = Rectangle(point - offset, offset * 2)

  local lookAtTarget = nil
  if target ~= nil then
    lookAtTarget = point - target
  end

  local nearest_waypoint = false
  local min_dist = math.huge
  local min_dot = math.huge

  if target ~= nil then
    for waypoint in pairs(self.spatialhash:objectsForRect(rect)) do
      local dist = point:dist(waypoint.circle.position)
      -- if this is the closest we've found and we have LOS, set this as the nearest
      local lookAt = waypoint.circle.position - point
      local dot = lookAt:dot(lookAtTarget)

      if dot < min_dot and dist < range and not self.collider:intersectsRay(point.x, point.y, lookAt.x, lookAt.y) then
        min_dot = dot
        nearest_waypoint = waypoint
      end
    end
  else
    for waypoint in pairs(self.spatialhash:objectsForRect(rect)) do
      local dist = point:dist(waypoint.circle.position)
      -- if this is the closest we've found and we have LOS, set this as the nearest
      local lookAt = waypoint.circle.position - point

      if dist < min_dist and dist < range and not self.collider:intersectsRay(point.x, point.y, lookAt.x, lookAt.y) then
        min_dist = dist
        nearest_waypoint = waypoint
      end
    end
  end

  return nearest_waypoint
end

-- Finds waypoint nearest to 'point' within 'range'
function Map:nearestWaypointInRange(point, range)
  local offset = vector(range, range)
  local rect = Rectangle(point - offset, offset * 2)

  local nearest_waypoint = false
  local min_dist = math.huge

  for waypoint in pairs(self.spatialhash:objectsForRect(rect)) do
    local dist = point:dist(waypoint.circle.position)
    -- if this is the closest we've found and we have LOS, set this as the nearest
    local lookAt = waypoint.circle.position - point

    if dist < min_dist and dist < range then
      min_dist = dist
      nearest_waypoint = waypoint
    end
  end

  return nearest_waypoint
end

function Map:saveWaypointGraph()
  local save_data = {}
  for id, waypoint in pairs(self.waypoints) do
    save_data[waypoint.id] = {
      id = waypoint.id,
      position = {
        x = waypoint.circle.position.x,
        y = waypoint.circle.position.y,
      },
      connections = {}
    }

    for connection_id, _ in pairs(waypoint.connections) do
      save_data[waypoint.id]['connections'][connection_id] = true
    end
  end

  local serpent = require 'serpent'
  local serialized = serpent.dump(save_data)
  local output_file = io.open('/Users/gloryfish/graph.txt', 'w')
  output_file:write(serialized)
  output_file:close()
end

function Map:loadWaypointGraph()
  local data = require 'resources/graph/graph'

  for id, info in pairs(data) do
    local waypoint = Waypoint(vector(info.position.x, info.position.y), tonumber(id))

    for connection_id, _ in pairs(info.connections) do
      waypoint.connections[connection_id] = true
    end
    self.waypoints[waypoint.id] = waypoint
    self.spatialhash:insertObjectForRect(waypoint, waypoint:getRect())
  end
end

function Map:addWall(points)
  table.insert(self.shapes, self.collider:addPolygon(unpack(self.polygon_points)))
end

function Map:saveWalls()
  local data = {}
  for index, shape in ipairs(self.shapes) do
    table.insert(data, {shape._polygon:unpack()})
  end

  local serpent = require 'serpent'
  local serialized = serpent.dump(data)
  local output_file = io.open('/Users/gloryfish/walls.txt', 'w')
  output_file:write(serialized)
  output_file:close()
end

function Map:loadWalls()
  local data = require 'resources/graph/walls'
  self.shapes = {}
  for index, wall_points in ipairs(data) do
    local shape = self.collider:addPolygon(unpack(wall_points))
    self.collider:setPassive(shape)
    table.insert(self.shapes, shape)
  end
end

-- HardonCollider delegate

function Map.on_collision(dt, shape_a, shape_b, mtv_x, mtv_y)
end

function Map.collision_stop()
end



-- AStar MapHandler

function Map:getNode(waypoint)

  if self.waypoints[waypoint.id] == nil then
    return nil
  end

  return Node(waypoint, 10, waypoint.id)
end


function Map:getAdjacentNodes(curnode, dest)
  local result = {}

  -- Process rooms according to door connections
  local currentWaypoint = self.waypoints[curnode.location.id]

  for neighbor_id, _ in pairs(currentWaypoint.connections) do
    local n = self:_handleNode(neighbor_id, curnode, dest.id)
    if n then
      table.insert(result, n)
    end
  end

  return result
end

function Map:locationsAreEqual(a, b)
  return a.id == b.id
end

function Map:_handleNode(id, fromnode, destid)
  -- Fetch a Node for the given location and set its parameters
  local waypoint = self.waypoints[id]
  local destWaypoint = self.waypoints[destid]

  local n = self:getNode(waypoint)

  if n ~= nil then
    local dx = math.max(waypoint.circle.position.x, destWaypoint.circle.position.x) - math.min(waypoint.circle.position.x, destWaypoint.circle.position.x)
    local dy = math.max(waypoint.circle.position.y, destWaypoint.circle.position.y) - math.min(waypoint.circle.position.y, destWaypoint.circle.position.y)
    local emCost = dx + dy

    n.mCost = n.mCost + fromnode.mCost
    n.score = n.mCost + emCost
    n.parent = fromnode

    return n
  end

  return nil
end

