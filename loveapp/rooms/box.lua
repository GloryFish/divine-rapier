--
--  room.lua
--  rogue-descent
--
--  Created by Jay Roberts on 2012-07-31.
--  Copyright 2012 Jay Roberts. All rights reserved.
--

require 'middleclass'
require 'vector'
require 'rectangle'
require 'colors'
require 'destination'
require 'choice'
require 'notifier'
require 'random'

Box = class('Box')

function Box:initialize(seed, destination, position, size)
  assert(type(seed) == 'number', 'seed must be a number')
  assert(instanceOf(Destination, destination), 'destination must be a Destination object')
  assert(position ~= nil, 'Box initialized without position')
  assert(size ~= nil, 'Box initialized without size')

  self.seed = seed
  self.destination = destination
  self.level = self.destination.level
  self.index = self.destination.index
  self.position = position
  self.size     = size
  self.center = position + size / 2

  self.playerSpot = vector(self.size.x / 2, self.size.y - 100)

  self:generate()
end

-- Base room generation.
function Box:generate()
  local randomizer = twister(self.seed)
  self.color = Color:random(randomizer)

  self.item = nil
  self.choices = {}

  if randomizer:random() > -0.05 then
    self.choices = Choice.randomPair(randomizer, self.level, vector(self.center.x - 200, self.center.y), vector(self.center.x + 100, self.center.y))
  else
    self.item = 'default item'
  end
end

function Box:receiveMessage(message, data)
  if message == 'world_click' then
    local position = data

    if self.choices[1].rect:contains(position) then
        local info = {
          choice = self.choices[1],
          destination = Destination(self.destination.level + 1, self.destination.index),
        }
        Notifier:postMessage('choice_selected', info)
        return
    end

    if self.choices[2].rect:contains(position) then
        local info = {
          choice = self.choices[2],
          destination = Destination(self.destination.level + 1, self.destination.index + 1),
        }
        Notifier:postMessage('choice_selected', info)
        return
    end

    return
  end
end

function Box:setIsCurrent(current)
  if self.isCurrent == current then
    return
  end

  self.isCurrent = current

  if self.isCurrent then
    Notifier:listenForMessage('world_click', self)
  else
    Notifier:stopListeningForMessage('world_click', self)
  end
end

-- Returns true if the room contains the provided world point
function Box:containsPoint(point)
  assert(vector.isvector(point), 'point must be a vector')

  return point.x >= self.position.x and
         point.x <= self.position.x + self.size.x and
         point.y >= self.position.y and
         point.y <= self.position.y + self.size.y
end


function Box:__tostring()
  return "Box ("..tonumber(self.destination.level)..","..tonumber(self.destination.index)..","..tonumber(self.destination.id)..") ("..tonumber(self.position.x)..","..tonumber(self.position.y)..")"
end

function Box:update(dt)
end


function Box:draw(mode)
  self.color:set()
  love.graphics.rectangle('fill', self.position.x, self.position.y, self.size.x, self.size.y)

  if self.item then
    -- self.item:draw()
    love.graphics.print(self.item, self.position.x + 200, self.position.y + 100)
  end

  for i, choice in ipairs(self.choices) do
    choice:draw()
  end
end
