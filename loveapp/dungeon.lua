--
--  dungeon.lua
--  rogue-descent
--
--  Created by Jay Roberts on 2012-02-24.
--  Copyright 2012 GloryFish.org. All rights reserved.
--

require 'middleclass'
require 'vector'
require 'roomfactory'
require 'notifier'
require 'random'

Dungeon = class('Dungeon')

function Dungeon:initialize(seed)
  self.seed = seed or 0
  self.roomFactory = RoomFactory(seed)
  self.rooms = {}

  self:reset()
end

function Dungeon:reset()
  print(string.format('Dungeon reset. Seed: %s', tostring(self.seed)))
  if self.currentRoom ~= nil then
    self.currentRoom:setIsCurrent(false)
  end

  self.rooms = {}
  self.roomSize = vector(640, 320)

  local startingRoom = self.roomFactory:buildRoom(Destination(1, 1), vector(0, 0), self.roomSize)

  self.rooms[startingRoom.destination.id] = startingRoom

  self.currentRoom = startingRoom
  startingRoom:setIsCurrent(true)

  -- This is a vector which indicates where an observer is currently looking, may not be the player's position
  -- Used for determining which rooms in the dungeon to render and update
  self.focus = startingRoom.position
end

function Dungeon:receiveMessage(message, data)
end

function Dungeon:idForLevelAndIndex(level, index)
  return ((level - 1) * level / 2) + index
end

function Dungeon:destinationForPosition(position)
  assert(vector.isvector(position), 'position must be a vector')

  local level = math.floor(position.y / self.roomSize.y) + 1

  local x = position.x + (self.roomSize.x * level / 2) - (self.roomSize.x / 2)
  local index = math.floor(x / self.roomSize.x) + 1

  if level < 1 then
    return nil
  end

  if index < 1 then
    return nil
  end

  if index > level then
    return nil
  end

  return Destination(level, index)
end

function Dungeon:positionForRoomAtDestination(destination)
  assert(instanceOf(Destination, destination), 'destination must be a Destination object')
  local level = destination.level
  local index = destination.index

  return vector((-level * self.roomSize.x / 2) + (self.roomSize.x / 2) + (self.roomSize.x * index) - self.roomSize.x,
                level * self.roomSize.y - self.roomSize.y)
end

function Dungeon:setCurrentRoom(destination)
  assert(instanceOf(Destination, destination), 'destination must be a Destination object')

  if self.currentRoom.destination ~= destination then
    local room = self:roomAt(destination)
    -- assert(instanceOf(Room, room), 'couldn\'t make a valid room')

    self.currentRoom:setIsCurrent(false)
    room:setIsCurrent(true)

    self.currentRoom = room
  end
end

function Dungeon:roomAt(destination)
  print(tostring(destination))
  assert(instanceOf(Destination, destination), 'destination must be a Destination object')

  local room = self.rooms[destination.id]
  if room == nil then
    local position = self:positionForRoomAtDestination(destination)
    room = self.roomFactory:buildRoom(destination, position, self.roomSize)
    self.rooms[destination.id] = room
  end

  return room
end

function Dungeon:update(dt)
  local focusDestination = self:destinationForPosition(self.focus)
  if not focusDestination then
    print('invalid focus destination for: ' .. tostring(self.focus))
    return
  end

  for index, destination in ipairs(self:getNeighborhood(focusDestination)) do
    self.rooms[destination.id]:update(dt)
  end
end

-- Gives a set of destinations (that actually have rooms) in an area around a given destination
function Dungeon:getNeighborhood(destination, spread)
 if spread == nil then
   spread = 3
 end

 local neighborhood = {}

 for level = destination.level - spread, destination.level + spread do
   for index = destination.index - spread, destination.index + spread do
     if level > 0 and index > 0 and index <= level then
       local dest = Destination(level, index)
       if self.rooms[dest.id] ~= nil then
         table.insert(neighborhood, dest)
       end
     end
   end
 end
 return neighborhood
end

-- Gives a set of destinations (which may or may not have rooms) which are direct descendants of a given destination
function Dungeon:getDestinationDescendants(destination, depth)
  assert(instanceOf(Destination, destination), 'destination must be a Destination object')

  if depth == nil then
    depth = 1
  end
  assert(depth > 0, 'depth must be greater than 0')

  local destinations = {}

  for level = destination.level + 1, destination.level + depth do
    for index = destination.index, destination.index + level - destination.level do
      local destination = Destination(level, index)
      table.insert(destinations, destination)
    end
  end

  return destinations
end

function Dungeon:draw(mode)
  local focusDestination = self:destinationForPosition(self.focus)
  if not focusDestination then
    print('invalid focus destination for: ' .. tostring(self.focus))
    return
  end

  for index, destination in ipairs(self:getNeighborhood(focusDestination, 2)) do
    self.rooms[destination.id]:draw(mode)
  end
end
