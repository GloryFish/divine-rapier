--
--  cursor.lua
--

require 'middleclass'
require 'vector'
require 'colors'

local Cursor = class('Cursor')

function Cursor:initialize()
  love.mouse.setVisible(false)
  self.spritesheet = sprites.ui
  self.scale = 1
  self.offset = vector(0, 0)

  self.quad_name = 'cursor_default.png'
end

function Cursor:update(dt)
  if self.quad_name == 'cursor_default.png' then
    self.offset = vector(0, 0)
  elseif self.quad_name == 'cursor_spell_enemy.png' then
    self.offset = vector(16, 16)
  end
end

function Cursor:draw()
  colors.white:set()
  self.spritesheet.batch:add(self.spritesheet.quads[self.quad_name],
                            love.mouse.getX(),
                            love.mouse.getY(),
                            0,
                            self.scale,
                            self.scale,
                            self.offset.x,
                            self.offset.y)
end

return Cursor()