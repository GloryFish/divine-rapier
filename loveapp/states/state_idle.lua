--
--  bot.lua
--

require 'middleclass'
require 'vector'

local State = class('StateIdle')

function State:initialize()
    self.name = 'state_idle'
end

function State:enter(actor)
  if vars.showai then
    print('Entered state: '..self.name)
  end
  actor:setMovement(vector(0, 0))
end

function State:execute(actor, dt)
end

function State:exit(actor)
  if vars.showai then
    print('Exiting state: '..self.name)
  end
end

StateIdle = State