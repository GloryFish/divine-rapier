
require 'middleclass'
require 'vector'
require 'states/state_bot_move_to_enemy'

local State = class('StateBotAttack')

function State:initialize()
  self.name = 'state_bot_attack'
end

function State:enter(actor)
  if vars.showai then
    print('Entered state: '..self.name)
  end

  actor:setMovement(vector(0, 0))
end

function State:execute(actor, dt)
  if actor.enemy.position:dist(actor.position) > actor.attack_range then
    actor:setState(StateBotMoveToEnemy())
  else
    actor.attack_elapsed = actor.attack_elapsed + dt
    if actor.attack_elapsed > actor.attack_max_delay then
      actor.attack:trigger(actor.enemy, actor.base_damage)
      actor.attack_elapsed = 0
    end
  end

  -- Map Area Effects
  for index, effect in ipairs(actor.map.area_effects) do
    if effect.name == 'stun_air' and actor.position:dist(effect.position) < effect.range then
      local state = StateBotStunned()
      state.duration = effect.duration
      state.air = true
      actor:setState(state)
    elseif effect.name == 'stun' and actor.position:dist(effect.position) < effect.range then
      local state = StateBotStunned()
      state.duration = effect.duration
      state.air = false
      actor:setState(state)
    elseif effect.name == 'damage' and actor.position:dist(effect.position) < effect.range then
      actor:takeDamage(effect.amount)
    end
  end

  -- Fleeing
  if actor.health_current / actor.health_max < actor.flee_threshhold then
    actor:setState(StateBotFleeing())
  end

  -- Dying
  if actor.health_current <= 0 then
    actor:setState(StateBotDying())
  end
end

function State:exit(actor)
  if vars.showai then
    print('Exiting state: '..self.name)
  end
end

StateBotAttack = State