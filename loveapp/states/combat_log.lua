--
--  combat_log.lua
--

require 'middleclass'
require 'circle'
require 'vector'
require 'colors'
require 'notifier'
require 'label'

CombatLog = class('CombatLog')

function CombatLog:initialize()
  self.sounds = {}
  self.sound_ids = {}

  local soundlist = love.filesystem.getDirectoryItems('resources/sounds')
  local count = 0
  for i, filename in ipairs(soundlist) do
    local ext = filename:sub(-3)
    if ext == 'wav' or ext == 'mp3' then
      local source = love.audio.newSource('resources/sounds/' .. filename, 'static')
      self.sounds[filename] = source
      table.insert(self.sound_ids, filename)
      count = count + 1
    end
  end

  print('Loaded '..count..' sounds.')
end

function CombatLog:update(dt)
end

function CombatLog:addLine(string)
end




function CombatLog:draw()
end
