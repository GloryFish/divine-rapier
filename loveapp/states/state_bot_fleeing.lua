
require 'middleclass'
require 'vector'

local State = class('StateBotFleeing')

function State:initialize()
  self.name = 'state_bot_fleeing'
end

function State:enter(actor)
  if vars.showai then
    print('Entered state: '..self.name)
  end

  actor.target = actor.map.dire_spawn
  actor.path = nil

  actor.thought:set('scared.png')
end

function State:execute(actor, dt)
  actor:setMovement(actor:getAIMovement(actor.target, actor.map))
  if actor.health_current == actor.health_max then
    actor:setState(StateBotSeekEnemy())
  end

  -- Map Area Effects
  for index, effect in ipairs(actor.map.area_effects) do
    if effect.name == 'stun_air' and actor.position:dist(effect.position) < effect.range then
      local state = StateBotStunned()
      state.duration = effect.duration
      state.air = true
      actor:setState(state)
    elseif effect.name == 'stun' and actor.position:dist(effect.position) < effect.range then
      local state = StateBotStunned()
      state.duration = effect.duration
      state.air = false
      actor:setState(state)

    elseif effect.name == 'damage' and actor.position:dist(effect.position) < effect.range then
      actor:takeDamage(effect.amount)
    end
  end

  -- Dying
  if actor.health_current <= 0 then
    actor:setState(StateBotDying())
  end
end

function State:exit(actor)
  if vars.showai then
    print('Exiting state: '..self.name)
  end
end

StateBotFleeing = State