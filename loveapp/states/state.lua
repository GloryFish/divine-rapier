--
--  bot.lua
--

require 'middleclass'
require 'vector'

local State = class('StateBase')


function State:initialize()
    self.name = 'state_base'
end

function State:enter(actor)
end

function State:execute(actor, dt)
end

function State:exit(actor)
end


StateBase = State