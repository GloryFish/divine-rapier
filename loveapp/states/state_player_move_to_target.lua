
require 'middleclass'
require 'vector'
require 'states/state_player_attack'

local State = class('StatePlayerMoveToTarget')

function State:initialize()
    self.name = 'state_player_move_to_target'
end

function State:enter(actor)
  if vars.showai then
    print('Entered state: '..self.name)
  end

  audio:playRandomSound(actor.sounds.moving, 'kunkka')
end

function State:execute(actor, dt)
  if actor.target == nil then
    actor:setState(StateIdle())
  elseif actor.target:dist(actor.position) < 20 then
    actor:setState(StateIdle())
  else
    actor:setMovement(actor:getAIMovement(actor.target, actor.map))
  end
end

function State:exit(actor)
  if vars.showai then
    print('Exiting state: '..self.name)
  end
end

StatePlayerMoveToTarget = State