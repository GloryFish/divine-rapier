
require 'middleclass'
require 'vector'
require 'states/state_idle'
require 'notifier'

local State = class('StateBotDead')

function State:initialize()
  self.name = 'state_bot_dead'
  self.dead_duration = 5
end

function State:enter(actor)
  if vars.showai then
    print('Entered state: '..self.name)
  end
  self.dead_time = love.timer.getTime()
  actor.position = vector(-10000, -10000)
end

function State:execute(actor, dt)
  if love.timer.getTime() - self. dead_time > self.dead_duration then
    -- Respawn
    actor.position = actor.map.dire_spawn
    actor.health_current = actor.health_max
    actor.enemy_last_position = nil
    actor.path = nil
    actor.rotation = 0
    actor:setState(StateBotSeekEnemy())
  end
end

function State:exit(actor)
  if vars.showai then
    print('Exiting state: '..self.name)
  end
end

StateBotDead = State