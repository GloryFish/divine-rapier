
require 'middleclass'
require 'vector'

local State = class('StatePlayerMoveToEnemy')

function State:initialize()
    self.name = 'state_player_move_to_enemy'
end

function State:enter(actor)
  if vars.showai then
    print('Entered state: '..self.name)
  end
end

function State:execute(actor, dt)
  if actor.selected_unit == nil then
    actor:setState(StateIdle())
  elseif actor.selected_unit.position:dist(actor.position) < actor.attack_range then
    actor:setState(StatePlayerAttack())
  else
    actor:setMovement(actor:getAIMovement(actor.selected_unit.position, actor.map))
  end
end

function State:exit(actor)
  if vars.showai then
    print('Exiting state: '..self.name)
  end
end

StatePlayerMoveToEnemy = State