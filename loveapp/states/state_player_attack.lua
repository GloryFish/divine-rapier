
require 'middleclass'
require 'vector'

local State = class('StatePlayerAttack')
local lastAtackSoundTime = love.timer.getTime()

function State:initialize()
    self.name = 'state_player_attack'
end

function State:enter(actor)
  if vars.showai then
    print('Entered state: '..self.name)
  end
  actor:setMovement(vector(0, 0))

  if love.timer.getTime() - lastAtackSoundTime > 5 then
    audio:playRandomSound(actor.sounds.attacking, 'kunkka')
    lastAtackSoundTime = love.timer.getTime()
  end
end

function State:execute(actor, dt)
  if actor.selected_unit == nil then
    actor:setState(StateIdle())
  elseif actor.selected_unit.position:dist(actor.position) > actor.attack_range then
    actor:setState(StatePlayerMoveToEnemy())
  else
    actor.attack_elapsed = actor.attack_elapsed + dt
    if actor.attack_elapsed > actor.attack_max_delay then
      actor.attack:trigger(actor.selected_unit, actor.base_damage)
      actor.attack_elapsed = 0
    end
  end

  -- Dying
  if actor.health_current <= 0 then
    actor:setState(StatePlayerDying())
  end
end

function State:exit(actor)
  if vars.showai then
    print('Exiting state: '..self.name)
  end
end

StatePlayerAttack = State