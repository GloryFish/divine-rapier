
require 'middleclass'
require 'vector'
require 'states/state_idle'
require 'states/state_bot_stunned'

local State = class('StateBotMoveToEnemy')

function State:initialize()
  self.name = 'state_bot_move_to_enemy'
end

function State:enter(actor)
  if vars.showai then
    print('Entered state: '..self.name)
  end
  actor.thought:set('exclamation.png')
end

function State:execute(actor, dt)
  local haveLOS = false
  -- Try to determine where the enemy is
  if actor.enemy ~= nil then
    local dist = actor.position:dist(actor.enemy.position)
    if dist < actor.vision_range and not actor.map:intersectsRay(actor.position, actor.enemy.position - actor.position) then
      haveLOS = true
      actor.enemy_last_position = actor.enemy.position
      actor.enemy_last_seen_time = love.timer.getTime()

      -- Keep the team notified of the enemy position
      actor.team:updateEnemyPosition(actor.enemy_last_position)
    end
  end

  if actor.enemy_last_position == nil -- We have no clue where th enemy could be
  or love.timer.getTime() - actor.enemy_last_seen_time > actor.memory_time -- Haven't seen the enemy in a while
  or (actor.path == nil or #actor.path == 0) and not haveLOS  -- We got there, but no enemy
  then
    actor:setState(StateBotSeekEnemy())

  elseif actor.enemy.position:dist(actor.position) < actor.attack_range and haveLOS then
    actor:setState(StateBotAttack())
  else
    -- If known enemy position is out of attack range, invalidate the path
    if actor.path ~= nil then
      local lastPathLocation = actor.path[#actor.path]

      if actor.enemy_last_position:dist(lastPathLocation) > actor.attack_range then
        actor.path = nil
      end
    end
    actor:setMovement(actor:getAIMovement(actor.enemy_last_position, actor.map))
  end

  -- Map Area Effects
  for index, effect in ipairs(actor.map.area_effects) do
    if effect.name == 'stun_air' and actor.position:dist(effect.position) < effect.range then
      local state = StateBotStunned()
      state.duration = effect.duration
      state.air = true
      actor:setState(state)
    elseif effect.name == 'stun' and actor.position:dist(effect.position) < effect.range then
      local state = StateBotStunned()
      state.duration = effect.duration
      state.air = false
      actor:setState(state)

    elseif effect.name == 'damage' and actor.position:dist(effect.position) < effect.range then
      actor:takeDamage(effect.amount)
    end
  end

  -- Fleeing
  if actor.health_current / actor.health_max < actor.flee_threshhold then
    actor:setState(StateBotFleeing())
  end

  -- Dying
  if actor.health_current <= 0 then
    actor:setState(StateBotDying())
  end
end

function State:exit(actor)
  if vars.showai then
    print('Exiting state: '..self.name)
  end
end

StateBotMoveToEnemy = State