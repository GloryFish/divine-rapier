
require 'middleclass'
require 'vector'

local State = class('StatePlayerDying')

function State:initialize()
  self.name = 'state_player_dying'
  self.dying_duration = 2
  self.startTime = love.timer.getTime()
end

function State:enter(actor)
  self.dying_start = love.timer.getTime()
  actor:setMovement(vector(0, 0))
  audio:playRandomSound(actor.sounds.death)
  audio:playSound('death.mp3')

  self.blood_particles = particles:add('death_blood')
  self.blood_particles:trigger(actor.position)

  Notifier:postMessage('player_died', actor)
end

function State:execute(actor, dt)
  local progress = (love.timer.getTime() - self.startTime) / self.dying_duration
  actor.scale = 1 - progress

  if love.timer.getTime() - self.dying_start > self.dying_duration then
    actor:setState(StatePlayerDead())
  end
end

function State:exit(actor)
  particles:remove(self.blood_particles)
  actor.scale = 1
  if vars.showai then
    print('Exiting state: '..self.name)
  end
end

StatePlayerDying = State