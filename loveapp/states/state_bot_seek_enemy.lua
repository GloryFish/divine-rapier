
require 'middleclass'
require 'vector'
require 'states/state_idle'
require 'notifier'

local State = class('StateBotSeekEnemy')

function State:initialize()
  self.name = 'state_bot_seek_enemy'
end

function State:enter(actor)
  if vars.showai then
    print('Entered state: '..self.name)
  end
  actor.path = nil
  actor.currentPatrolIndex = 1

  actor.thought:set('question.png')
end

function State:execute(actor, dt)
  local target = actor.patrol[actor.currentPatrolIndex]
  -- Reach patrol point, move to next
  if actor.position:dist(target) < 20 then
    actor.currentPatrolIndex = actor.currentPatrolIndex + 1
    if actor.currentPatrolIndex > #actor.patrol then
      actor.currentPatrolIndex = 1
    end
    target = actor.patrol[actor.currentPatrolIndex]
  end
  actor:setMovement(actor:getAIMovement(target, actor.map))

  -- Try to determine where the enemy is
  if actor.enemy ~= nil then
    local dist = actor.position:dist(actor.enemy.position)
    if dist < actor.vision_range and not actor.map:intersectsRay(actor.position, actor.enemy.position - actor.position) then
      actor.enemy_last_position = actor.enemy.position
      actor.enemy_last_seen_time = love.timer.getTime()

      -- After a moment, tell the team
      timer.add(math.random() * 2 + 1, function()
        actor.team:sawEnemyAt(actor.enemy_last_position)
      end)

      actor:setState(StateBotMoveToEnemy())
    end
  end

  -- Map Area Effects
  for index, effect in ipairs(actor.map.area_effects) do
    if effect.name == 'stun_air' and actor.position:dist(effect.position) < effect.range then
      local state = StateBotStunned()
      state.duration = effect.duration
      state.air = true
      actor:setState(state)
    elseif effect.name == 'stun' and actor.position:dist(effect.position) < effect.range then
      local state = StateBotStunned()
      state.duration = effect.duration
      state.air = false
      actor:setState(state)

    elseif effect.name == 'damage' and actor.position:dist(effect.position) < effect.range then
      actor:takeDamage(effect.amount)
    end
  end

  -- Fleeing
  if actor.health_current / actor.health_max < actor.flee_threshhold then
    actor:setState(StateBotFleeing())
  end

  -- Dying
  if actor.health_current <= 0 then
    actor:setState(StateBotDying())
  end
end

function State:exit(actor)
  if vars.showai then
    print('Exiting state: '..self.name)
  end
end

StateBotSeekEnemy = State