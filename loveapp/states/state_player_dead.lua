
require 'middleclass'
require 'vector'
require 'states/state_idle'
require 'notifier'

local State = class('StatePlayerDead')

function State:initialize()
  self.name = 'state_player_dead'
  self.dead_duration = 7
end

function State:enter(actor)
  self.dead_time = love.timer.getTime()
  actor.position = vector(-10000, -10000)
end

function State:execute(actor, dt)
  -- if love.timer.getTime() - self. dead_time > self.dead_duration then
  --   -- Respawn
  --   actor.position = actor.map.radiant_spawn
  --   actor.health_current = actor.health_max
  --   actor.rotation = 0
  --   actor:setState(StateIdle())
  --   Notifier:postMessage('player_respawned', actor)
  --   audio:playSound('respawn.mp3')
  -- end
end

function State:exit(actor)
end

StatePlayerDead = State