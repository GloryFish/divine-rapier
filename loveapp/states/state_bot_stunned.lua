
require 'middleclass'
require 'vector'
require 'notifier'

local State = class('StateBotStunned')

function State:initialize()
  self.name = 'state_bot_stunned'
  self.duration = 1
  self.air = true
end

function State:enter(actor)
  if vars.showai then
    print('Entered state: '..self.name)
  end

  self.stunned_start = love.timer.getTime()
  actor:setMovement(vector(0, 0))
end

function State:execute(actor, dt)
  local duration = love.timer.getTime() - self.stunned_start

  if self.air then
    actor.height = 75 *  math.sin(duration / self.duration * math.pi)
  end

  actor.rotation = actor.rotation + math.pi * 2 * dt * 1.5

  if duration > self.duration then
    actor:setState(StateBotMoveToEnemy())
  end

  -- Map Area Effects
  for index, effect in ipairs(actor.map.area_effects) do
    if effect.name == 'stun' and actor.position:dist(effect.position) < effect.range then
      local state = StateBotStunned()
      state.duration = effect.duration
      actor:setState(state)
    elseif effect.name == 'damage' and actor.position:dist(effect.position) < effect.range then
      actor:takeDamage(effect.amount)
    end
  end
end

function State:exit(actor)
  if vars.showai then
    print('Exiting state: '..self.name)
  end
  actor.height = 0
  actor.rotation = 0
end

StateBotStunned = State