require 'utility'

function weighted_selection(randomizer, items, filter)
  assert(#items > 0, 'No items available in set')

  local filtered_items = {}
  local total_weight = 0

  for index, item in ipairs(items) do
    if filter(item) then
      table.insert(filtered_items, item)
      total_weight = total_weight + item.weight
    end
  end

  assert(#filtered_items > 0, 'No items available after filtering')

  local threshhold = randomizer:random(0, total_weight)
  local last_choice = nil

  for index, item in ipairs(filtered_items) do
    threshhold = threshhold - item.weight
    if threshhold <= 0 then
      return item
    end
    last_choice = item
  end

  return last_choice
end


-- Return 'count' randomly selected items from the supplied table. Items are always returned as a table.
function random_sample(randomizer, items, count)
  assert(count <= #items, 'Cannot select more items than are contained in the list.')
  assert(count ~= 0, 'Must select at least one item')

  local selected = {}
  local options = shallowcopy(items)


  for i = 1, count do
    local index = randomizer:random(#options)
    table.insert(selected, table.remove(options, index))
  end
  return options
end