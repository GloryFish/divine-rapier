--
--  particles.lua
--

require 'vector'
require 'middleclass'

local Particles = class('Particles')

function Particles:initialize()
  self.effects = {}
  self.effect_ids = {}

  self.created_effects = {}

  local effectlist = love.filesystem.getDirectoryItems('resources/particles')
  local count = 0
  for i, filename in ipairs(effectlist) do
    local ext = filename:sub(-3)
    local name = filename:sub(1, -5)
    if ext == 'lua' then
      local effect = require('resources/particles/'..name)
      self.effects[name] = effect
      table.insert(self.effect_ids, name)
      count = count + 1
    end
  end

  print('Loaded '..count..' particle effects.')
end


function Particles:add(name)
  local effect = self.effects[name]()
  table.insert(self.created_effects, effect)
  return effect
end

function Particles:update(dt)
  for index, effect in ipairs(self.created_effects) do
    if effect.active then
      effect:update(dt)
    end
  end
end

function Particles:remove(to_remove)
  for index, effect in ipairs(self.created_effects) do
    if effect == to_remove then
      table.remove(self.created_effects, index)
    end
  end
end

function Particles:removeAll()
  self.created_effects = {}
end

function Particles:draw(layer)
  for index, effect in ipairs(self.created_effects) do
    if effect.active and effect.layer == layer then
      effect:draw(layer)
    end
  end
end

return Particles()