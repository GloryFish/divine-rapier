--
--  bot.lua
--

require 'middleclass'
require 'vector'
require 'colors'
require 'bar'
require 'thought'
require 'attack_melee'
require 'attack_ranged'
require 'states/state_idle'
require 'states/state_bot_dying'
require 'states/state_bot_dead'
require 'states/state_bot_fleeing'
require 'states/state_bot_move_to_enemy'
require 'states/state_bot_seek_enemy'
require 'states/state_bot_attack'

local heroes = require 'heroes'

local alreadyPicked = {}

Bot = class('Bot')

function Bot:initialize(map, team)
  self.map = map
  self.team = team

  self.hero = heroes[math.random(#heroes)]

  while in_table(self.hero.name, alreadyPicked) do
    self.hero = heroes[math.random(#heroes)]
  end
  table.insert(alreadyPicked, self.hero.name)

  print('initialized bot: '..self.hero.name)

  self.spritesheet = sprites.main
  self.size = self:getCurrentSize()
  self.position = vector(0, 0)
  self.target = self.position
  self.target = nil
  self.path = nil
  self.direction = vector(0, -1)

  self.bar = Bar(self, colors.green)

  self.thought = Thought(self)

  self.particles = {
    walk = particles:add('walk_dust'),
  }

  self.offset = vector(self.size.x / 2, self.size.y / 2)
  self.rotation = 0
  self.scale = 1
  self.flip = 1
  self.height = 0

  self.path = nil
  self.pathInterval = 0 -- Wait x seconds before generating a new path
  self.pathDuration = 1 -- how long since last path generation

  self.enemy = nil

  self.state = StateIdle()

  self.movement = vector(0, 0) -- This holds a vector containing the last movement input received
  self.velocity = vector(0, 0)

  -- seek_enemy state data
  self.enemy_last_position = nil
  self.memory_time = 5
  self.enemy_last_seen_time = love.timer.getTime() - self.memory_time * 2
  self.currentPatrolIndex = 1
  self.patrol = nil

  -- Stats
  self.vision_range = 200
  self.base_speed = 38 + math.random(15)
  self.health_max = 500
  self.health_current = 500
  self.level = 20

  self.flee_threshhold = 0.25

  self.attack_elapsed = 0

  if self.hero.attack == 'melee' then
    self.attack_range = 70
    self.attack_min_delay = 0.3
    self.attack_max_delay = 1.5
    self.base_damage = 20
    self.attack = AttackMelee(self, self.spritesheet.animations[self.hero.attack_animation])
  elseif self.hero.attack == 'ranged' then
    self.attack_range = 140
    self.attack_min_delay = 0.3
    self.attack_max_delay = 1.5
    self.base_damage = 15
    self.attack = AttackRanged(self, self.spritesheet.animations[self.hero.attack_animation])
  end

  self.sounds = {
    death = {
      'Kunk_kill_01.mp3',
      'Kunk_kill_02.mp3',
      'Kunk_kill_03.mp3',
      'Kunk_kill_04.mp3',
      'Kunk_kill_05.mp3',
      'Kunk_kill_06.mp3',
      'Kunk_kill_07.mp3',
      'Kunk_kill_08.mp3',
      'Kunk_kill_09.mp3',
      'Kunk_kill_10.mp3',
      'Kunk_kill_11.mp3',
      'Kunk_kill_12.mp3',
    },
  }

  if self.hero.name == 'Tidehunter' then
    self.sounds.death = {
      'Kunk_killspecial_01.mp3',
      'Kunk_killspecial_02.mp3',
      'Kunk_killspecial_03.mp3',
      'Kunk_killspecial_04.mp3',
      'Kunk_killspecial_05.mp3',
      'Kunk_killspecial_06.mp3',
    }
  end
end

function Bot:setState(state)
  if (self.state.name ~= state.name) then
    self.state:exit(self)
    self.state = state
    self.state:enter(self)
  end
end

function Bot:update(dt)
  local healthAmount = self.health_current / self.health_max
  self.bar.amount = healthAmount
  self.bar:update(dt)

  self.thought:update(dt)

  -- Walking dust particles
  if self.velocity.x ~= 0 and self.velocity.y ~= 0 then
    self.particles.walk:walk()
  else
    self.particles.walk:stop()
  end
  self.particles.walk.position = self.position

  self.attack:update(dt)

  self.state:execute(self, dt)

  -- Apply velocity to position
  self.position = self.position + self.velocity * dt
end

function Bot:takeDamage(amount)
  self.health_current = self.health_current - amount
  if self.health_current < 0 then
    self.health_current = 0

    if self.state.name ~= 'state_bot_dying' and self.state.name ~= 'state_bot_dead' then
      self:setState(StateBotDying())
    end
  end
end

function Bot:healDamage(amount)
  self.health_current = self.health_current + amount
  if self.health_current > self.health_max then
    self.health_current = self.health_max
  end
end

-- Call during update with a normalized movement vector
function Bot:setMovement(movement)
  self.movement = movement
  self.velocity.x = movement.x * self.base_speed
  self.velocity.y = movement.y * self.base_speed

  if movement.x ~= 0 and movement.y ~= 0 then
    self.direction = self.movement
  end
end

function Bot:getAIMovement(target, map)
  local movement = vector(0, 0)

  if self.path == nil then
    self.path = map:pathBetween(self.position, target, self.vision_range)
    if self.path == nil then
      return vector(0, 0)
    end
  end

  local nextLocation = self.path[1]

  -- Check to see if we've reached the current node
  if nextLocation ~= nil then
    if self.position:dist(nextLocation) < 10 then
      table.remove(self.path, 1)
      nextLocation = self.path[1]
    end
  end

  -- Move towards our current node
  if nextLocation ~= nil then
    local movement = nextLocation - self.position
    movement:normalize_inplace()
    return movement
  else
      self.path = nil
      return vector(0, 0)
  end

  return movement
end

function Bot:isDead()
  return self.state.name == 'state_bot_dying' or self.state.name == 'state_bot_dead'
end

-- Returns a vector representing the current size, based on the active quad
function Bot:getCurrentSize()
  local quad = self.spritesheet.quads[self.hero.frame]
  local x, y, w, h = quad:getViewport()
  return vector(w, h)
end

function Bot:draw()
  if self.state == 'dead' then
    return
  end

  colors.white:set()
  self.spritesheet.batch:add(self.spritesheet.quads['Shadow.png'],
                              math.floor(self.position.x),
                              math.floor(self.position.y) + 10,
                              0,
                              self.scale * self.flip,
                              self.scale,
                              20,
                              14)

  self.spritesheet.batch:add(self.spritesheet.quads[self.hero.frame],
                              math.floor(self.position.x),
                              math.floor(self.position.y) - self.height,
                              self.rotation,
                              self.scale * self.flip,
                              self.scale,
                              self.offset.x,
                              self.offset.y)

  colors.red:set()
  -- self.shape:draw()

  if self.state == 'dying' then
    return
  end
  self.bar:draw()

  self.thought:draw()

  self.attack:draw()

  -- Draw patrol
  if vars.showpatrols then
    colors.pink:set()
    local patrol_points = {}
    for index, point in ipairs(self.patrol) do
      table.insert(patrol_points, point.x)
      table.insert(patrol_points, point.y)
    end
    if #patrol_points > 3 then
      love.graphics.polygon('line', patrol_points)
    end
  end

  -- Draw vision range
  if vars.showai then
    colors.gray:set()
    love.graphics.circle('line', self.position.x, self.position.y, self.vision_range, 45)

    -- Draw last known position
    if self.enemy_last_position ~= nil and love.timer.getTime() - self.enemy_last_seen_time < self.memory_time then
      colors.orange:alpha(255 - ((love.timer.getTime() - self.enemy_last_seen_time) / self.memory_time * 255)):set()
      love.graphics.circle('line', self.enemy_last_position.x, self.enemy_last_position.y, 20, 30)
      love.graphics.line(self.enemy_last_position.x, self.enemy_last_position.y, self.position.x, self.position.y)
    end
  end

end