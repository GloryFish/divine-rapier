

require 'logger'
require 'vector'
require 'colors'
require 'rectangle'
require 'wall'
require 'dude'
require 'spatialhash'

local scene = Gamestate.new()

function scene:enter(pre)
  love.graphics.setBackgroundColor(colors.lightgray:unpack())

  self.spatialhash = SpatialHash(64)

  self.spawn = vector(50, 50)

  self.walls = {}
  -- Outer walls
  self.walls[1] = Wall(vector(0, 0), vector(20, love.graphics.getHeight()))
  self.walls[2] = Wall(vector(love.graphics.getWidth() - 20, 0), vector(20, love.graphics.getHeight()))
  self.walls[3] = Wall(vector(20, 0), vector(love.graphics.getWidth() - 40, 20))
  self.walls[4] = Wall(vector(20, love.graphics.getHeight() - 20), vector(love.graphics.getWidth() - 40, 20))

  -- Boxes
  self.walls[5] = Wall(vector(200, 200), vector(200, 80))
  self.walls[6] = Wall(vector(200, 400), vector(200, 80))
  self.walls[7] = Wall(vector(650, 200), vector(80, 280))

  for index, wall in ipairs(self.walls) do
    self.spatialhash:insertObjectForRect(wall, wall.rectangle)
  end

  self.dudes = {}

  for i = 1, 1 do
    -- local dude = Dude(self.spawn:clone())
    -- table.insert(self.dudes, dude)

    -- self.spatialhash:insertObjectForRect(dude, dude:getRectangle())
  end

end

function scene:keypressed(key, unicode)
  if key == 'escape' then
    self:quit()
  end
end

function scene:mousepressed(x, y, button)
    local dude = Dude(vector(x, y))
    table.insert(self.dudes, dude)
    self.spatialhash:insertObjectForRect(dude, dude:getRectangle())
end

function scene:mousereleased(x, y, button)
end

function scene:update(dt)
  if love.mouse.isDown('l') then
  end

  for index, dude in ipairs(self.dudes) do
    dude:update(dt)
  end
end

function scene:draw()
  for index, wall in ipairs(self.walls) do
    wall:draw()
  end

  local mousepoint = vector(love.mouse.getX(), love.mouse.getY())
  local neighborhood = self.spatialhash:objectsForPoint(mousepoint)


  for index, dude in ipairs(self.dudes) do
    dude:draw()
  end


  --draw debug stuff
  if false then
    for index, object in ipairs(neighborhood) do
      object:draw(colors.red)
    end

    colors.red:set()
    for x = 1, love.graphics.getWidth(), self.spatialhash.cell_size do
      love.graphics.line(x, 0, x, love.graphics.getHeight())
    end

    for y = 1, love.graphics.getHeight(), self.spatialhash.cell_size do
      love.graphics.line(0, y, love.graphics.getWidth(), y)
    end
  end
end

function scene:quit()
end

function scene:leave()
end

return scene