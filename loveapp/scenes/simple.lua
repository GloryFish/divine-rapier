--
--  simple.lua
--  rogue-descent
--
--  A simple scene.
--
--  Created by Jay Roberts on 2012-02-23.
--  Copyright 2012 Jay Roberts. All rights reserved.
--

require 'logger'
require 'vector'
require 'colors'
require 'rectangle'
require 'destination'
require 'camera'
require 'gesturerecognizer'
require 'notifier'
require 'player'

local scene = Gamestate.new()

function scene:enter(pre)
  self.dungeon = Dungeon(1)
  self.camera = Camera()
  self.camera.deadzone = 5
  self.camera.smoothMovement = true
  self.gestureRecognizer = GestureRecognizer()

  Notifier:listenForMessage('mouse_drag', self)
  Notifier:listenForMessage('mouse_click', self)
  Notifier:listenForMessage('choice_selected', self)

  self.logger = Logger(vector(10, 10))

  self.player = Player()


  self:reset()
end

function scene:reset()
  self.dungeon:reset()

  local roomCenter = vector(self.dungeon.currentRoom.size.x / 2, self.dungeon.currentRoom.size.y / 2)
  self.camera.position = roomCenter
  self.camera.focus = roomCenter

  self.player.position = self.dungeon.currentRoom.position + self.dungeon.currentRoom.playerSpot
end

function scene:receiveMessage(message, data)

  if message == 'mouse_drag' then
    local position = data
    self.camera.smoothMovement = false
    self.camera.focus = self.camera.focus - position
  end

  if message == 'mouse_click' then
    local position = data
    local worldPoint = self.camera:screenToWorld(position)
    Notifier:postMessage('world_click', worldPoint)
  end

  if message == 'choice_selected' then
    local info = data

    self.dungeon:setCurrentRoom(info.destination)

    local roomCenter = self.dungeon.currentRoom.position + vector(self.dungeon.currentRoom.size.x / 2, self.dungeon.currentRoom.size.y / 2)
    self.camera.smoothMovement = true
    self.camera.focus = roomCenter

    self.player.position = self.dungeon.currentRoom.position + self.dungeon.currentRoom.playerSpot
    self.player:step()
  end

end

function scene:keypressed(key, unicode)
  if key == 'escape' then
    self:quit()
  end
end

function scene:mousepressed(x, y, button)
end

function scene:mousereleased(x, y, button)
end

function scene:update(dt)
  self.logger:update(dt)
  self.logger:addLine(string.format('Age: %i', self.player.age))
  self.logger:addLine(string.format('Life: %i', self.player.life))
  self.logger:addLine(string.format('Money: $%i', self.player.money))
  self.logger:addLine(string.format('Happiness: %i', self.player.happiness))
  self.logger:addLine(string.format('Foresight: %i', self.player.foresight))

  self.gestureRecognizer:update(dt)

  self.dungeon.focus = self.camera.position
  self.dungeon:update(dt)

  self.player:update(dt)

  self.camera:update(dt)
end

function scene:draw()
  self.camera:apply()

  self.dungeon:draw()

  self.player:draw()

  love.graphics.draw(sprites.main.batch)

  self.camera:unapply()

  self.logger:draw()

end

function scene:quit()
  love.event.push('quit')
end

function scene:leave()
end

return scene