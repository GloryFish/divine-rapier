--
--  cutscene_start.lua
--

require 'logger'
require 'vector'
require 'colors'
require 'rectangle'
require 'timer'
require 'menu'
require 'combat_log'
require 'camera'

local scene = Gamestate.new()

function scene:enter(pre)
  love.graphics.setBackgroundColor(0, 0, 0)

  self.camera = Camera()
  self.camera.position = vector(love.graphics.getWidth() / 2, love.graphics.getHeight() / 2)
  self.camera.focus = self.camera.position
  self.camera.handheld = false
  self.camera.deadzone = 0
  self.camera.smoothMovement = false
  self.camera.useBounds = false

  local coins = love.audio.newSource('resources/sounds/buy.mp3', 'static')
  love.audio.play(coins)

  self.combatlog = CombatLog()
  self.combatlog.position = vector(20, love.graphics.getHeight() - 260)
  self.combatlog:addPurchase({ui_color = Color(37, 107, 224), name = 'Kunkka (YeomanNinja)'}, 'Divine Rapier')

  self.music = love.audio.newSource('resources/music/battle_march.mp3', 'stream')
  self.music:setLooping(true)
  self.music:setVolume(0.5)
  timer.add(1.75, function()
    love.audio.play(self.music)
    Gamestate.switch(scenes.divine)
    scenes.divine.music = self.music
    scenes.divine.combatlog = self.combatlog
  end)
end

function scene:keypressed(key, unicode)

end

function scene:mousepressed(x, y, button)
end

function scene:mousereleased(x, y, button)
end

function scene:update(dt)
  cursor:update(dt)

  timer.update(dt)
  tween.update(dt)

  self.camera:update(dt)

end

function scene:draw()
  sprites.ui.batch:clear()

  self.camera:apply()

  love.graphics.clear()


  self.camera:unapply()

  self.combatlog:draw()

  cursor:draw()
  love.graphics.draw(sprites.ui.batch)
end


function scene:leave()
end

return scene