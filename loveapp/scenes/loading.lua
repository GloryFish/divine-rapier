--
--  loading.lua
--  rogue-descent
--
--  Created by Jay Roberts on 2012-04-26.
--  Copyright 2012 Jay Roberts. All rights reserved.
--

require 'logger'
require 'vector'
require 'colors'
require 'rectangle'
require 'shaders'

local scene = Gamestate.new()

function scene:enter(pre)
  self.finished = false
  self:load()
end

function scene:keypressed(key, unicode)
end

function scene:mousepressed(x, y, button)
end

function scene:mousereleased(x, y, button)
end

function scene:load()
  local startTime = os.time()

  -- Set up vars
  vars = {
    debug = false,
    showhud = true,
    showgraph = false,
    showhash  = false,
    showwalls = false,
    showpatrols = false,
    showstats = false,
    showai = false,
    editor = false,
    sound = true,
    lognotifications = false,
  }

  -- Prepare fonts
  fonts = {
    default        = love.graphics.newFont('resources/fonts/Ubuntu-R.ttf', 20),
    small        = love.graphics.newFont('resources/fonts/Ubuntu-R.ttf', 14),
  }

  -- Prepare canvases
  canvases = {
    main = love.graphics.newCanvas()
  }

  -- Prepare spritesheet
  sprites = require 'spritesheets'

  -- Create console
  console = require 'console'

  -- Create stats
  stats = require 'stats'

  -- Load effects
  shaders = require 'shaders'

  -- Load particle systems
  particles = require 'particles'

  -- Load timer library
  timer = require 'timer'

  -- Load tweening library
  tween = require 'tween'


  cursor = require 'cursor'

  current_effect = 'vision'



  -- Load sound and music
  audio = require 'audio'

  timer = require 'timer'

  local modes = love.window.getFullscreenModes()
  table.sort(modes, function(a, b) return a.width*a.height < b.width*b.height end)
  local mode = modes[#modes]
  for i, m in ipairs(modes) do
    if m.width == 1280 and m.height == 720 then
      mode = m
      break
    elseif m.width <= 1280 then
      mode = m
    end
  end
  local modeflags = {
    fullscreen = false,
    vsync = true,
    msaa = 0,
  }

  love.window.setMode(mode.width, mode.height, modeflags)

  local totalTime = os.time() - startTime
  print('Loaded in '..tostring(totalTime)..' seconds')
end

function scene:update(dt)
  self.finished = true

  if self.finished then
    Gamestate.switch(scenes.title)
  end
end

function scene:draw()
end

function scene:quit()
end

function scene:leave()
end

return scene