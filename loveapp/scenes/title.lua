--
--  title.lua
--

require 'logger'
require 'vector'
require 'colors'
require 'rectangle'
require 'timer'
require 'menu'
require 'textbutton'
require 'camera'

local scene = Gamestate.new()

function scene:enter(pre)
  self.camera = Camera()
  self.camera.position = vector(love.graphics.getWidth() / 2, love.graphics.getHeight() / 2)
  self.camera.focus = self.camera.position
  self.camera.handheld = false
  self.camera.deadzone = 0
  self.camera.smoothMovement = false
  self.camera.useBounds = false

  self.imageTitle = love.graphics.newImage('resources/sprites/title_divine.png')
  self.positionTitle = vector(love.graphics.getWidth() / 2 - self.imageTitle:getWidth() / 2, (love.graphics.getHeight() / 2) - 200)


  self.imageSubtitle = love.graphics.newImage('resources/sprites/title_monkey_monks.png')
  self.positionSubtitle = vector((love.graphics.getWidth() / 2 - self.imageSubtitle:getWidth() / 2) + 210, (love.graphics.getHeight() / 2) - 120)
  self.subtitleColor = colors.white:alpha(0)

  self.imageRapier = love.graphics.newImage('resources/sprites/title_rapier.png')
  self.positionRapier = vector((love.graphics.getWidth() / 2 - self.imageRapier:getWidth() / 2), -1 * self.imageRapier:getHeight())

  self.imageClouds = love.graphics.newImage('resources/sprites/title_clouds.png')
  self.positionClouds = vector(0, 0)
  self.scaleClouds = love.graphics.getHeight() / 720
  self.cloudSpeed = 20

  self.monkLogo = love.graphics.newImage('resources/sprites/monklogo.png')
  self.sixpackLogo = love.graphics.newImage('resources/sprites/sixpacklogo.png')
  self.monkAddress = love.graphics.newImage('resources/sprites/address_mm.png')
  self.sixpackAddress = love.graphics.newImage('resources/sprites/address_sixpack.png')
  self.createdby = love.graphics.newImage('resources/sprites/createdby.png')

  self.positionLogos = vector(love.graphics.getWidth() - 430, love.graphics.getHeight() - 120)

  -- Fade in title screen
  self.overlayColor = colors.black:clone()
  timer.add(1, function()
    tween.start(3, self.overlayColor, { a = 0 })
  end)

  -- Drop rapier
  timer.add(3.5, function()
    tween.start(1.5, self.positionRapier, { y = (love.graphics.getHeight() / 2) - 360 }, 'outBounce')
  end)
  timer.add(4, function()
    self.camera:shake(0.7, 0.2)
  end)

  -- Fade in subtitle
  timer.add(5.5, function()
    tween.start(1.5, self.subtitleColor, { a = 255 })
  end)


  -- Play title music
  self.music = love.audio.newSource('resources/music/battle_in_the_winter.mp3', 'stream')
  self.music:setLooping('true')
  love.audio.play(self.music)

  self.swordSound = love.audio.newSource('resources/sounds/title_sword_ring.mp3', 'static')

  -- Fade in subtitle
  timer.add(3.5, function()
    love.audio.play(self.swordSound)
  end)

  self.menu = Menu(vector(love.graphics.getWidth() / 2, love.graphics.getHeight() / 2 + 50))
  local playButton = TextButton('Play')
  playButton.action = function()
  self:startGame()
  end
  self.menu:addButton(playButton)

  local fullscreenButton = TextButton('Fullscreen')
  local width, height, modeflags = love.window.getMode()
  if modeflags.fullscreen then
    fullscreenButton.text = 'Windowed'
  else
    fullscreenButton.text = 'Fullscreen'
  end
  fullscreenButton.action = function()
    local width, height, modeflags = love.window.getMode()
    love.window.setFullscreen(not modeflags.fullscreen)
    local width, height, modeflags = love.window.getMode()
    if modeflags.fullscreen then
      fullscreenButton.text = 'Windowed'
    else
      fullscreenButton.text = 'Fullscreen'
    end
  end
  self.menu:addButton(fullscreenButton)

  local quitButton = TextButton('Quit')
  quitButton.action = function()
    self:quit()
  end
  self.menu:addButton(quitButton)
end

function scene:keypressed(key, unicode)
  if key == 'escape' then
    self:quit()
  end

end

function scene:mousepressed(x, y, button)
end

function scene:mousereleased(x, y, button)
  self.menu:mousereleased(vector(x, y))
end

function scene:update(dt)
  cursor:update(dt)

  timer.update(dt)
  tween.update(dt)

  self.camera:update(dt)

  self.menu:update(dt)

  self.positionClouds.x = self.positionClouds.x - self.cloudSpeed * dt
  self.positionClouds.y = (math.sin(love.timer.getTime() / 5) + 1) * 30 * -1

  if self.positionClouds.x < self.imageClouds:getWidth() * self.scaleClouds * -1 then
    self.positionClouds.x = self.positionClouds.x + self.imageClouds:getWidth() * self.scaleClouds
  end
end

function scene:draw()
  sprites.ui.batch:clear()

  self.camera:apply()

  colors.white:set()

  love.graphics.clear(29, 31, 55)
  love.graphics.draw(self.imageRapier, self.positionRapier.x, self.positionRapier.y)

  self.camera:unapply()

  love.graphics.setBlendMode('multiply')

  self.camera:apply()

  love.graphics.draw(self.imageClouds, self.positionClouds.x, self.positionClouds.y, 0, self.scaleClouds, self.scaleClouds)
  love.graphics.draw(self.imageClouds, self.positionClouds.x + (self.imageClouds:getWidth() * self.scaleClouds), self.positionClouds.y, 0, self.scaleClouds, self.scaleClouds)
  love.graphics.draw(self.imageClouds, self.positionClouds.x + (self.imageClouds:getWidth() * 2 * self.scaleClouds), self.positionClouds.y, 0, self.scaleClouds, self.scaleClouds)

  self.camera:unapply()

  love.graphics.setBlendMode('alpha')

  self.camera:apply()

  love.graphics.draw(self.imageTitle, self.positionTitle.x, self.positionTitle.y)

  self.subtitleColor:set()
  love.graphics.draw(self.imageSubtitle, self.positionSubtitle.x, self.positionSubtitle.y)
  love.graphics.draw(self.sixpackLogo, self.positionLogos.x, self.positionLogos.y, 0, 0.5)
  love.graphics.draw(self.monkLogo, self.positionLogos.x + 250, self.positionLogos.y, 0, 0.5)
  love.graphics.draw(self.sixpackAddress, self.positionLogos.x - 35, self.positionLogos.y + 85)
  love.graphics.draw(self.monkAddress, self.positionLogos.x + 220, self.positionLogos.y + 85)
  love.graphics.draw(self.createdby, 25, self.positionLogos.y + 80)

  -- Draw menu
  self.menu:draw()

  -- Draw overlay

  self.overlayColor:set()
  love.graphics.rectangle('fill', 0, 0, love.graphics.getWidth(), love.graphics.getHeight())

  self.camera:unapply()

  cursor:draw()
  love.graphics.draw(sprites.ui.batch)

end

function scene:startGame()
  local fadeOutTime = 3

  local startTime = love.timer.getTime()
  timer.do_for(fadeOutTime, function(dt)
    local progress = (love.timer.getTime() - startTime) / 3
    self.music:setVolume(1 - progress)
  end,
  function() end)
  tween.start(fadeOutTime, self.overlayColor, { a = 255 }, 'linear', function()
    self.music:stop()
    Gamestate.switch(scenes.cutscene_start)
  end)

end

function scene:quit()
  tween.start(1, self.overlayColor, { a = 255 }, 'linear', function() love.event.push('quit') end)
end

function scene:leave()
end

return scene