--
--  divine.lua
--

require 'logger'
require 'vector'
require 'colors'
require 'rectangle'
require 'map'
require 'gesturerecognizer'
require 'notifier'
require 'bot'
require 'bot_team'
require 'ray'
require 'reticle'
require 'states/state_idle'
require 'spell_manager'
require 'hud'
require 'combat_log'

local scene = Gamestate.new()

scene.difficulty = 3

function scene:enter(pre)
  love.graphics.setBackgroundColor(0, 0, 0)

  console.delegate = self

  self.map = Map()

  self.camera = Camera()
  self.camera.position = vector(670, 500)
  self.camera.focus = vector(670, 500)
  self.camera.deadzone = 0
  self.camera.smoothMovement = false
  self.camera.handheld = false
  self.camera.scale = 1.65
  self.camera.useBounds = true
  self.camera.bounds = {
    top = -150,
    left = -150,
    bottom = 1174,
    right = 1174,
  }

  self.map.camera = self.camera

  self.logger = Logger()
  self.logger.position.y = 55

  particles:removeAll()

  self.player = Player()
  self.player.position = vector(750, 495)
  self.player.target = self.player.position
  self.player.map = self.map

  self.bot_team = BotTeam()

  self.bots = {}
  local bot_heroes = {}
  for i = 1, 2 + self.difficulty do
    local bot = Bot(self.map)
    bot.position = self.map.dire_spawn
    bot.map = self.map
    self.bot_team:addPlayer(bot)
    bot.enemy = self.player
    bot:setState(StateIdle())
    table.insert(self.bots, bot)
    table.insert(bot_heroes, bot.hero)
  end

  self.bot_team:setState('team_hunt_enemy')

  self.fountain_healing = 10

  self.gesturerecognizer = GestureRecognizer()

  self.reticle = Reticle()

  self.hud = Hud()
  self.hud:setRadiantHeroes({self.player.hero})
  self.hud:setDireHeroes(bot_heroes)

  self.hud.radiantScore = 23
  self.hud.direScore = 30

  self.spellmanager = SpellManager({SpellTorrent(self.player), SpellTidebringer(self.player), SpellXMarks(self.player), SpellGhostShip(self.player)})

  self.combatlog = CombatLog()
  self.combatlog.position = vector(20, love.graphics.getHeight() - 260)

  -- Gold timer
  timer.addPeriodic(0.8, function()
    self.player.gold = self.player.gold + 1
    self.hud.goldLabel.text = tostring(self.player.gold)
  end)

  Notifier:listenForMessage('mouse_down', self)
  Notifier:listenForMessage('mouse_down_right', self)
  Notifier:listenForMessage('mouse_click', self)
  Notifier:listenForMessage('player_died', self)
  Notifier:listenForMessage('player_respawned', self)
  Notifier:listenForMessage('bot_died', self)

  self.curtainColor = colors.black:clone()
  tween.start(2, self.curtainColor, { a = 0 }, 'linear')

  -- Gameover stuff
  self.gameStartTime = love.timer.getTime()
  self.kills = 0
  self.gameover = false
  self.youdiedColor = colors.white:alpha(0)
  self.youdiedImage = love.graphics.newImage('resources/sprites/youdied.png')
  self.gameoverText = ''
  self.gameoverFont = love.graphics.newFont('resources/fonts/Cinzel-R.ttf', 24)
  self.gameoverTextColor = Color(251, 228, 201):alpha(0)

  self.menu = nil
end

function scene:keypressed(key, unicode)
  if console:keypressed(key, unicode) then
    return
  end

  if key == '`' then
    console:open()
  end

  if key == 'f' then
    local width, height, modeflags = love.window.getMode()
    love.window.setFullscreen(not modeflags.fullscreen)
  end

  Notifier:postMessage('key_pressed', key)

  if vars.editor then
    if key == 'a' then
      self.map.mode = 'add_waypoint'
    end
    if key == 'c' and self.map.selectedWaypoint ~= nil then
      self.map.mode = 'connect_waypoint'
    end
    if key == 'v' and self.map.selectedWaypoint ~= nil then
      self.map.mode = 'disconnect_waypoint'
    end
    if key == 'm' and self.map.selectedWaypoint ~= nil then
      self.map.mode = 'move_waypoint'
    end
    if key == 'p' and self.map.mode ~= 'add_polygon' then
      self.map.polygon_points = {}
      self.map.mode = 'add_polygon'
    elseif key == 'p' and self.map.mode == 'add_polygon' then
      self.map:addWall(self.map.polygon_points)
      self.map.polygon_points = {}
      self.map.mode = 'normal'
    end
    if key == 'o' and self.map.mode == 'add_polygon' then
      self.map.polygon_points = {}
      self.map.mode = 'normal'
    end
    if key == 'd' and self.map.selectedWaypoint ~= nil then
      local waypoint = self.map.selectedWaypoint
      for id, _ in pairs(waypoint.connections) do
        local secondWaypoint = self.map.waypoints[id]
        self.map:disconnectWaypoints(waypoint, secondWaypoint)
      end

      self.map.spatialhash:removeObjectForRect(waypoint, waypoint:getRect())
      self.map.waypoints[waypoint.id] = nil
      self.map.mode = 'normal'
    end
    if key == 's' then
      self.map:saveWaypointGraph()
    end
  end
end

function scene:mousepressed(x, y, button)
end

function scene:mousereleased(x, y, button)
  if self.menu then
    self.menu:mousereleased(vector(x, y))
  end
end

function scene:receiveMessage(message, data)
  if message == 'mouse_click' then
    local position = data
    local worldPoint = self.camera:screenToWorld(position)

    -- Pass this on to other interested game objects
    Notifier:postMessage('world_click', worldPoint)

    if self.map.mode == 'add_polygon' then
      print('adding point to polygon: '..tostring(worldPoint))
      table.insert(self.map.polygon_points, worldPoint.x)
      table.insert(self.map.polygon_points, worldPoint.y)
    end

  elseif message == 'mouse_down' then
    local position = data
    local worldPoint = self.camera:screenToWorld(position)

    -- Pass this on to other interested game objects
    Notifier:postMessage('world_down', worldPoint)

    -- check to see if player clicked a bot
    for index, bot in ipairs(self.bots) do
      if worldPoint:dist(bot.position) < 20 then
        -- Pass this on to other interested game objects
        Notifier:postMessage('unit_targeted', bot)
      end
    end

  elseif message == 'mouse_down_right' then
    local position = data
    local worldPoint = self.camera:screenToWorld(position)

    -- Pass this on to other interested game objects
    Notifier:postMessage('world_down_right', worldPoint)

    -- check to see if player right-clicked a bot
    for index, bot in ipairs(self.bots) do
      if worldPoint:dist(bot.position) < 20 then
        -- Pass this on to other interested game objects
        Notifier:postMessage('unit_selected', bot)
      end
    end

    if self.map:pointIsWalkable(worldPoint) then
      self.reticle:show(worldPoint)
    end
  elseif message == 'player_died' then
    self.spellmanager.enabled = false
    self:showGameover()

  elseif message == 'player_respawned' then
    local player = data
    self.camera.position = player.position
    self.spellmanager.enabled = true
  elseif message == 'bot_died' then
    local bot = data
    local gold = 0 + 200 + (bot.level * 9)
    self.player.gold = self.player.gold + gold
    self.combatlog:addKill(self.player.hero, bot.hero, gold)
    self.hud.radiantScore = self.hud.radiantScore + 1
  end
end

function scene:update(dt)
  timer.update(dt)
  tween.update(dt)

  cursor:update(dt)
  stats:update(dt)
  console:update(dt)

  self.logger:update(dt)
  self.gesturerecognizer:update(dt)

  self.map:update(dt)

  local mouse = vector(love.mouse.getX(), love.mouse.getY())
  local cam_movement = vector(0, 0)
  if mouse.x <= 0 then
    cam_movement = cam_movement + vector(-1, 0)
  end
  if mouse.x >= love.graphics.getWidth() - 1 then
    cam_movement = cam_movement + vector(1, 0)
  end
  if mouse.y <= 0 then
    cam_movement = cam_movement + vector(0, -1)
  end
  if mouse.y >= love.graphics.getHeight() - 1 then
    cam_movement = cam_movement + vector(0, 1)
  end
  self.camera:setMovement(cam_movement)
  self.camera:update(dt)

  local world = self.camera:screenToWorld(vector(love.mouse.getX(), love.mouse.getY()))
  self.logger:addLine('Screen: '..tostring(vector(love.mouse.getX(), love.mouse.getY())))
  self.logger:addLine('World: '..tostring(world))
  self.logger:addLine('Scale: '..tostring(self.camera.scale))
  self.logger:addLine('Mode: '..self.map.mode)

  if vars.showai then
    self.logger:addLine('Team State: '..self.bot_team.state)
  end

  self.player:update(dt)

  particles:update(dt)

  self.bot_team:update(dt)
  for index, bot in ipairs(self.bots) do
  -- Fountain healing/damage
    if bot.position:dist(self.map.dire_spawn) < 25 then
      bot:healDamage(self.fountain_healing * dt)
    end
    if bot.position:dist(self.map.radiant_spawn) < 25 then
      bot:takeDamage(self.fountain_healing * dt)
    end

    bot:update(dt)
    if vars.showai then
      self.logger:addLine('AI State: '..bot.state.name)
    end
  end

  -- Fountain healing/damage
  if self.player.position:dist(self.map.radiant_spawn) < 25 then
    self.player:healDamage(self.fountain_healing * dt)
  end
  if self.player.position:dist(self.map.dire_spawn) < 25 then
    self.player:takeDamage(self.fountain_healing * dt)
  end



  table.sort(self.bots, function(a, b) return a.position.y < b.position.y end)

  self.reticle:update(dt)

  self.hud:update(dt)

  self.spellmanager:update(dt)

  self.combatlog:update(dt)

  if self.menu ~= nil then
    self.menu:update(dt)
  end
end

function scene:showGameover()
  -- Fade out background music
  local fadeOutTime = 3
  local startTime = love.timer.getTime()
  timer.do_for(fadeOutTime, function(dt)
    local progress = (love.timer.getTime() - startTime) / 3
    self.music:setVolume(1 - progress)
  end,
  function()
    self.music:stop()
  end)

  -- Fade curtain in slowly
  tween.start(7, self.curtainColor, { a = 255 }, 'linear', function()
      self.menu = Menu(vector(love.graphics.getWidth() / 2, love.graphics.getHeight() / 2 + 100))

      local playAgain = TextButton('Play Again')
      playAgain.action = function()
        Gamestate.switch(scenes.cutscene_start)
      end
      self.menu:addButton(playAgain)

      local backToMenu = TextButton('Main Menu')
      backToMenu.action = function()
        Gamestate.switch(scenes.title)
      end
      self.menu:addButton(backToMenu)
  end)

  -- Fade youdied in fast
  tween.start(2, self.youdiedColor, { a = 255 }, 'linear')

  timer.add(2, function()
    tween.start(4, self.gameoverTextColor, { a = 255 }, 'linear')
  end)

  self.gameover = true

  -- Get gameover text
  local gameTime = love.timer.getTime() - self.gameStartTime - 13
  if gameTime < 2 then
    gameTime = 2
  end
  self.gameoverText = string.format('But, you survived %d seconds longer than Tom did when he tried the same thing.\n\nMatch ID #117157288\n\nWatch it, it\'s hilarious.', gameTime)
end


function scene:draw()
  sprites.main.batch:clear()
  sprites.ui.batch:clear()

  self.camera:apply()

  self.map:draw()

  self.reticle:draw()

  self.spellmanager:drawEffects()

  self.player:drawSelection('below')

  for index, bot in ipairs(self.bots) do
    bot:draw()
  end

  self.player:drawSelection('above')

  self.player:draw()


  colors.white:set()

  particles:draw('below')

  love.graphics.draw(sprites.main.batch)

  particles:draw('above')

  self.camera:unapply()

  stats:draw()
  console:draw()
  self.logger:draw()

  self.spellmanager:drawIcons()

  self.hud:draw()

  self.spellmanager:drawOverlays()

  love.graphics.draw(sprites.ui.batch)

  self.spellmanager:drawText()
  self.hud:drawText()

  self.curtainColor:set()
  love.graphics.rectangle('fill', 0, 0, love.graphics.getWidth(), love.graphics.getHeight())

  self.youdiedColor:set()
  if self.gameover then
    love.graphics.draw(self.youdiedImage, love.graphics.getWidth() / 2 - self.youdiedImage:getWidth() / 2, love.graphics.getHeight() / 2 - 300)

    love.graphics.setFont(self.gameoverFont)
    colors.white:alpha(self.gameoverTextColor.a):set()
    love.graphics.printf(self.gameoverText, love.graphics.getWidth() / 2 - 240, love.graphics.getHeight() / 2 - 200 - 1, 480, 'center')

    colors.black:alpha(self.gameoverTextColor.a):set()
    love.graphics.printf(self.gameoverText, love.graphics.getWidth() / 2 - 240, love.graphics.getHeight() / 2 - 200 + 1, 480, 'center')

    self.gameoverTextColor:set()
    love.graphics.printf(self.gameoverText, love.graphics.getWidth() / 2 - 240, love.graphics.getHeight() / 2 - 200, 480, 'center')

    if self.menu ~= nil then
      self.menu:draw()
    end
  end

  sprites.ui.batch:clear()
  cursor:draw()
  love.graphics.draw(sprites.ui.batch)

  self.combatlog:draw()
end

function scene:quit()
  local fadeOutTime = 2

  local startTime = love.timer.getTime()
  timer.do_for(fadeOutTime, function(dt)
    local progress = (love.timer.getTime() - startTime) / 3
    self.music:setVolume(1 - progress)
  end,
  function() end)
  tween.start(fadeOutTime, self.curtainColor, { a = 255 }, 'linear', function()
    self.music:stop()
    Gamestate.switch(scenes.title)
  end)
end

function scene:leave()
end

return scene