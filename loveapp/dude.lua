--
--  door.lua
--  rogue-descent
--
--  Created by Jay Roberts on 2012-02-23.
--  Copyright 2012 Jay Roberts. All rights reserved.
--

require 'middleclass'
require 'vector'
require 'rectangle'
require 'colors'
require 'utility'
require 'notifier'
require 'rectangle'
require 'notifier'
require 'circle'

Dude = class('Dude')

local states = {}

states.idle = {}
function states.idle:update(dt, dude)
end


states.walk_forward = {}
function states.walk_forward:enter()
end
function states.walk_forward:update(dt, dude)
    dude.circle.position = dude.circle.position + dude.direction * dude.speed * dt
end

states.spin_around = {}
function states.spin_around:enter()
  self.elapsed = 0
  self.spinTime = math.random(1, 5)
  self.speed = 4
end
function states.spin_around:update(dt, dude)
  self.elapsed = self.elapsed + dt
  if self.elapsed > self.spinTime then
    dude:changeState(states.walk_forward)
  end

  dude.direction:rotate_inplace(dt * self.speed)
end

function Dude:changeState(newState)
  self.state = newState
  self.state:enter()
end

function Dude:initialize(position)
  assert(position ~= nil, 'Dude initialized without position')
  self.circle = Circle(position, 15)

  self:changeState(states.spin_around)
  self.speed = 50
  self.direction = vector(0, -1)

end

function Dude:getRectangle()
  local position = self.circle.position
  local radius = self.circle.radius
  return Rectangle(vector(position.x - radius, position.y - radius), vector(radius * 2, radius * 2))
end

function Dude:update(dt)
  self.state:update(dt, self)
end

function Dude:draw(color)
  if color then
    color:set()
  else
    colors.white:set()
  end
  self.circle:draw(vector(0, 0), 'fill')

  local indicator = self.direction * self.circle.radius

  colors.red:set()
  love.graphics.line(self.circle.position.x, self.circle.position.y, self.circle.position.x + indicator.x, self.circle.position.y + indicator.y)

end