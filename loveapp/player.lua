--
--  player.lua
--  rogue-descent
--
--  Created by Jay Roberts on 2012-02-27.
--  Copyright 2012 GloryFish.org. All rights reserved.
--

require 'middleclass'
require 'vector'
require 'colors'
require 'notifier'
require 'bar'
require 'attack_melee'

require 'states/state_idle'
require 'states/state_player_move_to_target'
require 'states/state_player_move_to_enemy'
require 'states/state_player_dying'
require 'states/state_player_dead'

Player = class('Player')

function Player:initialize()
  self.spritesheet = sprites.main
  self.size = self:getCurrentSize()
  self.position = vector(0, 0)
  self.target = self.position
  self.direction = vector(0, -1)

  self.bar = Bar(self, colors.green)

  self.attack = AttackMelee(self, sprites.main.animations.melee_slash)

  self.selected_unit = nil

  self.hero = {
    frame = 'Kunkka_icon.png',
    name = 'Kukka (YeomanNinja)',
    attack = 'melee',
    attack_animation = 'melee_slash',
    ui_icon = 'kunkka.png',
  }

  self.particles = {
    walk = particles:add('walk_dust'),
  }

  -- stats
  self.base_speed = 60
  self.vision_range = 200
  self.health_max = 500
  self.health_current = 500
  self.attack_range = 70
  self.attack_max_delay = 1
  self.base_damage = 100
  self.gold = 4
  self.level = 20

  self.attack_elapsed = 0

  self.state = StateIdle()
  self.movement = vector(0, 0) -- This holds a vector containing the last movement input received
  self.velocity = vector(0, 0)

  self.offset = vector(self.size.x / 2, self.size.y / 2)
  self.rotation = 0
  self.scale = 1
  self.flip = 1
  self.height = 0

  Notifier:listenForMessage('world_down_right', self)
  Notifier:listenForMessage('unit_selected', self)

  self.sounds = {
    moving = {
      'Kunk_move_01.mp3',
      'Kunk_move_02.mp3',
      'Kunk_move_03.mp3',
      'Kunk_move_04.mp3',
      'Kunk_move_05.mp3',
      'Kunk_move_06.mp3',
      'Kunk_move_07.mp3',
      'Kunk_move_08.mp3',
      'Kunk_move_09.mp3',
      'Kunk_move_10.mp3',
      'Kunk_move_11.mp3',
    },
    attacking = {
      'Kunk_attack_01.mp3',
      'Kunk_attack_02.mp3',
      'Kunk_attack_03.mp3',
      'Kunk_attack_04.mp3',
      'Kunk_attack_05.mp3',
      'Kunk_attack_06.mp3',
      'Kunk_attack_07.mp3',
      'Kunk_attack_08.mp3',
      'Kunk_attack_09.mp3',
      'Kunk_attack_10.mp3',
      'Kunk_attack_11.mp3',
    },
    death = {
      'Kunk_death_01.mp3',
      'Kunk_death_02.mp3',
      'Kunk_death_03.mp3',
      'Kunk_death_04.mp3',
      'Kunk_death_05.mp3',
      'Kunk_death_06.mp3',
      'Kunk_death_07.mp3',
      'Kunk_death_08.mp3',
      'Kunk_death_09.mp3',
      'Kunk_death_10.mp3',
      'Kunk_death_11.mp3',
      'Kunk_death_12.mp3',
      'Kunk_death_13.mp3',
      'Kunk_death_14.mp3',
      'Kunk_death_15.mp3',
      'Kunk_death_16.mp3',
      'Kunk_death_17.mp3',
      },
  }

end

function Player:receiveMessage(message, data)
  if self:isDead() then
    return
  end
  if message == 'world_down_right' then
    local worldPoint = data
    self:setTargetPoint(worldPoint)
    self.selected_unit = nil
  end
  if message == 'unit_selected' then
    local enemy = data
    if self.selected_unit ~= enemy then
      self:setState(StatePlayerMoveToEnemy())
      self.selected_unit = enemy
    end
  end
end

function Player:setState(state)
  if (self.state.name ~= state.name) then
    self.state:exit(self)
    self.state = state
    self.state:enter(self)
  end
end

function Player:setTargetPoint(target)
  if self.state.name ~= 'state_player_dying' and self.state.name ~= 'state_player_dead' then
    self.target = target
    self.path = nil
    self:setState(StatePlayerMoveToTarget())
  end
end

-- Call during update with a normalized movement vector
function Player:setMovement(movement)
  self.movement = movement
  self.velocity.x = movement.x * self.base_speed
  self.velocity.y = movement.y * self.base_speed

  if movement.x ~= 0 and movement.y ~= 0 then
    self.direction = self.movement
  end
end

function Player:update(dt)
  if self.health_current == 0 and self.state.name ~= 'state_player_dying' and self.state.name ~= 'state_player_dead' then
    self:setState(StatePlayerDying())
  end

  -- Walk dust particles
  if self.velocity.x ~= 0 and self.velocity.y ~= 0 then
    self.particles.walk:walk()
  else
    self.particles.walk:stop()
  end
  self.particles.walk.position = self.position

  self.bar.amount = self.health_current / self.health_max
  self.bar:update(dt)

  self.attack:update(dt)

  self.state:execute(self, dt)

  -- Apply velocity to position
  self.position = self.position + self.velocity * dt
end

function Player:getAIMovement(target, map)
  local movement = vector(0, 0)

  if self.path == nil then
    self.path = map:pathBetween(self.position, target, self.vision_range)
    if self.path == nil then
      self:setState(StateIdle())
      return vector(0, 0)
    end
  end

  local nextLocation = self.path[1]

  -- Check to see if we've reached the current node
  if nextLocation ~= nil then
    if self.position:dist(nextLocation) < 10 then
      table.remove(self.path, 1)
      nextLocation = self.path[1]
    end
  end

  -- Move towards our current node
  if nextLocation ~= nil then
    local movement = nextLocation - self.position
    movement:normalize_inplace()
    return movement
  else
      self:setState(StateIdle())
      return vector(0, 0)
  end

  return movement
end

function Player:takeDamage(amount)
  self.health_current = self.health_current - amount
  if self.health_current < 0 then
    self.health_current = 0

    if  self.state.name ~= 'state_player_dying' and self.state.name ~= 'state_player_dead' then
      self:setState(StatePlayerDying())
    end
  end
end

function Player:healDamage(amount)
  self.health_current = self.health_current + amount
  if self.health_current > self.health_max then
    self.health_current = self.health_max
  end
end

-- Returns a vector representing the current size, based on the active quad
function Player:getCurrentSize()
  local quad = self.spritesheet.quads['Kunkka_icon.png']
  local x, y, w, h = quad:getViewport()
  return vector(w, h)
end

function Player:isDead()
  return self.state.name == 'state_player_dying' or self.state.name == 'state_player_dead'
end

function Player:draw()
  if self.state == 'dead' then
    return
  end

  colors.white:set()
  self.spritesheet.batch:add(self.spritesheet.quads['Shadow.png'],
                              math.floor(self.position.x),
                              math.floor(self.position.y) + 10,
                              0,
                              self.scale * self.flip,
                              self.scale,
                              20,
                              14)

  self.spritesheet.batch:add(self.spritesheet.quads[self.hero.frame],
                              math.floor(self.position.x),
                              math.floor(self.position.y) - self.height,
                              self.rotation,
                              self.scale * self.flip,
                              self.scale,
                              self.offset.x,
                              self.offset.y)

  if self.state.name == 'state_player_dying' then
    return
  end


  self.bar:draw()

  self.attack:draw()

  if vars.showai then
    colors.green:set()
    love.graphics.line(self.position.x, self.position.y, (self.position + self.direction * 50):unpack())
  end
end


function Player:drawSelection(layer)
  if self.selected_unit == nil then
    return
  end

  local quad = 'selection_ring_red_upper.png'
  if layer == 'above' then
    quad = 'selection_ring_red_lower.png'
  end

  colors.white:set()
  self.spritesheet.batch:add(self.spritesheet.quads[quad],
                              math.floor(self.selected_unit.position.x),
                              math.floor(self.selected_unit.position.y) + 10,
                              0,
                              1,
                              1,
                              20,
                              14)
end