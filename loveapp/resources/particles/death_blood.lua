--
-- particle_effect.lua
--

require 'middleclass'
require 'vector'

local ParticleEffect = class('ParticleEffect')

function ParticleEffect:initialize()
  self.systems = {}
  self.position = vector(0, 0)
  self.layer = 'above'
  self.active = true
  self.rate = 40
  self.duration = 1

  -- Create and configure the systems that are part of this effect
  local bloodImage = love.graphics.newImage('resources/sprites/blood.png');
  local system = love.graphics.newParticleSystem(bloodImage, 50)

  system:setEmissionRate(self.rate)
  system:setSpeed(-20)
  system:setLinearAcceleration(0, 20, 0, 20)
  system:setSizes(0, 0.7)
  system:setColors(255, 255, 255, 255, 255, 255, 255, 225, 58, 128, 255, 0)
  system:setPosition(self.position.x, self.position.y)
  system:setEmitterLifetime(1)
  system:setParticleLifetime(1, 1)
  system:setDirection(math.pi / 2)
  system:setRotation(0, math.pi)
  system:setSpread(math.pi * 0.75)

  table.insert(self.systems, system)

  local bloodImage = love.graphics.newImage('resources/sprites/blood_splatter.png');
  local system_splatter = love.graphics.newParticleSystem(bloodImage, 50)

  system_splatter:setEmissionRate(self.rate)
  system_splatter:setSpeed(-20, 0)
  system_splatter:setLinearAcceleration(0, 0, 0, 0)
  system_splatter:setSizes(0.5, 0.7)
  system_splatter:setColors(255, 255, 255, 255, 255, 255, 255, 225, 58, 128, 255, 0)
  system_splatter:setPosition(self.position.x, self.position.y)
  system_splatter:setEmitterLifetime(1)
  system_splatter:setParticleLifetime(1, 1)
  system_splatter:setDirection(math.pi / 2)
  system_splatter:setRotation(0, math.pi)
  system_splatter:setSpread(math.pi * 2)

  table.insert(self.systems, system_splatter)
end

function ParticleEffect:trigger(position)
  self.position = position
  for index, system in ipairs(self.systems) do
    system:start()
  end
end

function ParticleEffect:update(dt)
  for index, system in ipairs(self.systems) do
    system:setPosition(self.position.x, self.position.y)
    system:update(dt)
  end
end


function ParticleEffect:draw()
  for index, system in ipairs(self.systems) do
    love.graphics.draw(system)
  end
end

return ParticleEffect