--
-- particle_effect.lua
--

require 'middleclass'
require 'vector'

local ParticleEffect = class('ParticleEffect')

function ParticleEffect:initialize()
  self.systems = {}
  self.position = vector(0, 0)
  self.layer = 'below'
  self.active = true

  -- Create and configure the systems that are part of this effect
  local splashImage = love.graphics.newImage('resources/sprites/spray.png');
  local system = love.graphics.newParticleSystem(splashImage, 400)

  system:setEmissionRate(100)
  system:setSpeed(50, 0)
  system:setLinearAcceleration(0, 0, 0, 0)
  system:setSizes(0.7, 1.5)
  system:setColors(255, 255, 255, 255, 91, 173, 230, 0)
  system:setPosition(self.position.x, self.position.y)
  system:setEmitterLifetime(1)
  system:setParticleLifetime(1, 1)
  system:setSpread(math.pi * 2)

  table.insert(self.systems, system)
end

function ParticleEffect:trigger(position)
  self.position = position
  for index, system in ipairs(self.systems) do
    system:setPosition(position.x, position.y)
    system:start()
  end
end

function ParticleEffect:stop()
  for index, system in ipairs(self.systems) do
    system:stop()
  end
end

function ParticleEffect:update(dt)
  for index, system in ipairs(self.systems) do
    -- TODO self.particles:setPosition(self.position.x, self.position.y)
    system:setPosition(self.position.x, self.position.y)
    system:update(dt)
  end
end


function ParticleEffect:draw()
  for index, system in ipairs(self.systems) do
    love.graphics.draw(system)
  end
end

return ParticleEffect