--
-- particle_effect.lua
--

require 'middleclass'
require 'vector'

local ParticleEffect = class('ParticleEffect')

function ParticleEffect:initialize()
  self.systems = {}
  self.position = vector(0, 0)
  self.layer = 'below'
  self.active = true
  self.rate = 5
  self.duration = 4

  -- Create and configure the systems that are part of this effect
  local splashImage = love.graphics.newImage('resources/sprites/splash.png');
  local system = love.graphics.newParticleSystem(splashImage, 500)

  system:setEmissionRate(self.rate)
  system:setSpeed(0, 0)
  system:setLinearAcceleration(0, 0, 0, 0)
  system:setSizes(0.0, 0.4)
  system:setColors(255, 255, 255, 190, 58, 128, 255, 0)
  system:setPosition(self.position.x, self.position.y)
  system:setEmitterLifetime(self.duration)
  system:setParticleLifetime(1, 1)
  system:setDirection(math.pi / 2)
  system:setRotation(0, math.pi)
  system:setSpread(20)

  table.insert(self.systems, system)
end

function ParticleEffect:splash(position)
  self.position = position
  for index, system in ipairs(self.systems) do
    system:start()
  end
end

function ParticleEffect:update(dt)
  for index, system in ipairs(self.systems) do
    -- TODO self.particles:setPosition(self.position.x, self.position.y)
    system:setPosition(self.position.x, self.position.y)
    system:update(dt)
  end
end


function ParticleEffect:draw()
  for index, system in ipairs(self.systems) do
    love.graphics.draw(system)
  end
end

return ParticleEffect