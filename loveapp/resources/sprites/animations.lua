local animations = {

  melee_slash = {
    rotation = math.pi / 2,
    frames = {
      {
        name = 'Slice_1.png', -- this is a Zwoptex sprite name
        duration = 0.03 -- duration in seconds
      },
      {
        name = 'Slice_2.png', -- this is a Zwoptex sprite name
        duration = 0.03 -- duration in seconds
      },
      {
        name = 'Slice_3.png', -- this is a Zwoptex sprite name
        duration = 0.03 -- duration in seconds
      },
      {
        name = 'Slice_4.png', -- this is a Zwoptex sprite name
        duration = 0.03 -- duration in seconds
      },
      {
        name = 'Slice_5.png', -- this is a Zwoptex sprite name
        duration = 0.03 -- duration in seconds
      },
      {
        name = 'Slice_6.png', -- this is a Zwoptex sprite name
        duration = 0.03 -- duration in seconds
      },
      {
        name = 'Slice_7.png', -- this is a Zwoptex sprite name
        duration = 0.03 -- duration in seconds
      },
      {
        name = 'Slice_8.png', -- this is a Zwoptex sprite name
        duration = 0.03 -- duration in seconds
      },
      {
        name = 'Slice_9.png', -- this is a Zwoptex sprite name
        duration = 0.03 -- duration in seconds
      },
      -- Add new frames here
    }
  },

  ranged_fireball = {
    rotation = 2 * math.pi, -- Pointed down
    scale = 0.5,
    frames = {
      {
        name = 'Ranged_Fire_01.png', -- this is a Zwoptex sprite name
        duration = 0.03 -- duration in seconds
      },
    },
  },

  ranged_venom = {
    rotation = 2 * math.pi, -- Pointed down
    scale = 0.5,
    frames = {
      {
        name = 'Ranged_Venom_01.png', -- this is a Zwoptex sprite name
        duration = 0.03 -- duration in seconds
      },
    },
  },

  ranged_arrow = {
    rotation = -math.pi / 4, -- Angled down to the left
    scale = 0.5,
    frames = {
      {
        name = 'Ranged_Arrow_01.png', -- this is a Zwoptex sprite name
        duration = 0.03 -- duration in seconds
      },
    },
  },

  ranged_spear = {
    rotation = -math.pi / 4, -- Angled down to the left
    scale = 1,
    frames = {
      {
        name = 'Ranged_Spear_01.png', -- this is a Zwoptex sprite name
        duration = 0.03 -- duration in seconds
      },
    },
  },

  ranged_axe = {
    rotation = -math.pi / 4,
    scale = 0.5,
    frames = {
      {
        name = 'Ranged_Axe_01.png', -- this is a Zwoptex sprite name
        duration = 0.2 -- duration in seconds
      },
      {
        name = 'Ranged_Axe_02.png', -- this is a Zwoptex sprite name
        duration = 0.2 -- duration in seconds
      },
      {
        name = 'Ranged_Axe_03.png', -- this is a Zwoptex sprite name
        duration = 0.2 -- duration in seconds
      },
      {
        name = 'Ranged_Axe_04.png', -- this is a Zwoptex sprite name
        duration = 0.2 -- duration in seconds
      },
    },
  },

  ranged_ice_star = {
    rotation = 0,
    scale = 0.5,
    frames = {
      {
        name = 'Ranged_IceStar_01.png', -- this is a Zwoptex sprite name
        duration = 0.2 -- duration in seconds
      },
    },
  },

  ranged_power_purple = {
    rotation = -math.pi / 4, -- Angled down to the left
    scale = 0.5,
    frames = {
      {
        name = 'Ranged_Power_Purple_01.png', -- this is a Zwoptex sprite name
        duration = 0.2 -- duration in seconds
      },
    },
  },

  ranged_power_white = {
    rotation = -math.pi / 4, -- Angled down to the left
    scale = 0.5,
    frames = {
      {
        name = 'Ranged_Power_White_01.png', -- this is a Zwoptex sprite name
        duration = 0.2 -- duration in seconds
      },
    },
  },

  ranged_power_blue = {
    rotation = -math.pi / 4, -- Angled down to the left
    scale = 0.5,
    frames = {
      {
        name = 'Ranged_Power_Blue_01.png', -- this is a Zwoptex sprite name
        duration = 0.2 -- duration in seconds
      },
    },
  },

  ranged_power_yellow = {
    rotation = -math.pi / 4, -- Angled down to the left
    scale = 0.5,
    frames = {
      {
        name = 'Ranged_Power_Yellow_01.png', -- this is a Zwoptex sprite name
        duration = 0.2 -- duration in seconds
      },
    },
  },

  ranged_power_green = {
    rotation = -math.pi / 4, -- Angled down to the left
    scale = 0.5,
    frames = {
      {
        name = 'Ranged_Power_Green_01.png', -- this is a Zwoptex sprite name
        duration = 0.2 -- duration in seconds
      },
    },
  },

  ranged_power_teal = {
    rotation = -math.pi / 4, -- Angled down to the left
    scale = 0.5,
    frames = {
      {
        name = 'Ranged_Power_Teal_01.png', -- this is a Zwoptex sprite name
        duration = 0.2 -- duration in seconds
      },
    },
  },

  ranged_fire_rock = {
    rotation = -math.pi / 4, -- Angled down to the left
    scale = 0.5,
    frames = {
      {
        name = 'Ranged_FireRock_01.png', -- this is a Zwoptex sprite name
        duration = 0.2 -- duration in seconds
      },
    },
  },

  ranged_templar = {
    rotation = -math.pi / 4, -- Angled down to the left
    scale = 0.5,
    frames = {
      {
        name = 'Ranged_Templar_01.png', -- this is a Zwoptex sprite name
        duration = 0.2 -- duration in seconds
      },
    },
  },

  ranged_glaive = {
    rotation = 0,
    scale = 0.5,
    frames = {
      {
        name = 'Ranged_Glaive_01.png', -- this is a Zwoptex sprite name
        duration = 0.2 -- duration in seconds
      },
    },
  },

  ranged_electro = {
    rotation = -math.pi / 4, -- Angled down to the left
    scale = 0.5,
    frames = {
      {
        name = 'Ranged_Electro.png', -- this is a Zwoptex sprite name
        duration = 0.2 -- duration in seconds
      },
    },
  },

  ranged_spear_green = {
    rotation = -math.pi / 4, -- Angled down to the left
    scale = 1,
    frames = {
      {
        name = 'Ranged_Spear_Green.png', -- this is a Zwoptex sprite name
        duration = 0.2 -- duration in seconds
      },
    },
  },

  ranged_pa = {
    rotation = -math.pi / 4, -- Angled down to the left
    scale = 1,
    frames = {
      {
        name = 'Ranged_PA.png', -- this is a Zwoptex sprite name
        duration = 0.2 -- duration in seconds
      },
    },
  },

}

return animations