--
--  slash.lua
--

require 'middleclass'
require 'vector'
require 'colors'

local rangedSounds = {
  'follow_thru01.mp3',
  'follow_thru02.mp3',
  'follow_thru03.mp3',
  'thump01.mp3',
  'thump02.mp3',
  'thump03.mp3',
}

AttackRanged = class('AttackRanged')

function AttackRanged:initialize(attachedTo, animation)
  self.spritesheet = sprites.main
  self.animation = animation

  self.currentFrameIndex = 1

  self.position = attachedTo.position
  self.offset = self:getCurrentSize() / 2

  self.scale = 0.5
  self.flip = 1
  self.rotation = math.atan2(attachedTo.direction:unpack())

  self.elapsed = 0
  self.speed = 120

  self.direction = vector(0, -1)

  self.attachedTo = attachedTo
  self.color = color

  self.moving = false

  self.damage = 10
end

function AttackRanged:getCurrentSize()
  local currentFrame = self.animation.frames[self.currentFrameIndex]
  local quad = self.spritesheet.quads[currentFrame.name]
  local x, y, w, h = quad:getViewport()
  return vector(w, h)
end

function AttackRanged:trigger(target, damage)
  if target:isDead() then
    return
  end
  self.target = target
  self.damage = damage
  self.position = self.attachedTo.position
  self.moving = true
  self.elapsed = 0
  self.targetPosition = vector(0, 0)
end

function AttackRanged:cancel()
  self.moving = false
end

function AttackRanged:update(dt)
  if self.moving then
    if not self.target:isDead() then
      self.targetPosition = self.target.position
    end
    self.direction = self.targetPosition - self.position
    self.direction:normalize_inplace()

    self.position = self.position + self.direction * self.speed * dt

    if self.position:dist(self.target.position) < 5 then
      self.moving = false
      self.target:takeDamage(self.damage)
      audio:playRandomSound(rangedSounds)
    end

    self.rotation = math.atan2(self.direction:unpack())

    self.elapsed = self.elapsed + dt

    -- Handle animation
    if #self.animation.frames > 1 then -- More than one frame
      local duration = self.animation.frames[self.currentFrameIndex].duration

      if self.elapsed > duration then -- Switch to next frame
        self.currentFrameIndex = self.currentFrameIndex + 1
        if self.currentFrameIndex > #self.animation.frames then -- Aaaand back around
          self.currentFrameIndex = 1
          self.slashing = false
        end
        self.elapsed = self.elapsed - duration
      end
    end
  end
end

function AttackRanged:draw()
  if self.moving then
    colors.white:alpha(0.5 * 255):set()
    local currentFrame = self.animation.frames[self.currentFrameIndex]
    self.spritesheet.batch:add(self.spritesheet.quads[currentFrame.name],
                                math.floor(self.position.x),
                                math.floor(self.position.y),
                                self.animation.rotation - self.rotation,
                                self.animation.scale * self.flip,
                                self.animation.scale,
                                self.offset.x,
                                self.offset.y)
  end
end