--
--  waypoint_graph.lua
--

require 'middleclass'
require 'vector'
require 'colors'
require 'utility'
require 'waypoint'
require 'spatialhash'
require 'waypoint'

WaypointGraph = class('WaypointGraph')

function WaypointGraph:initialize()
  self.waypoints = {}
  self.selectedWaypoint = {}
end


function WaypointGraph:connectWaypoints(a, b)
  if a == b then
    return
  end

  a:connectTo(b.id)
  b:connectTo(a.id)
end

function WaypointGraph:disconnectWaypoints(a, b)
  if a == b then
    return
  end

  a:disconnectFrom(b.id)
  b:disconnectFrom(a.id)
end

function WaypointGraph:draw()
    for index, waypoint in pairs(self.waypoints) do
    local isSelected = waypoint == self.selectedWaypoint
    waypoint:draw(isSelected)

    for id, _ in pairs(waypoint.connections) do
      if id < waypoint.id then
        local secondWaypoint = self.waypoints[id]
        colors.blue:set()
        love.graphics.line(waypoint.circle.position.x, waypoint.circle.position.y,
                          secondWaypoint.circle.position.x, secondWaypoint.circle.position.y)
      end
    end
  end
end
