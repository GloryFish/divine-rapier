--
--  spell_manager.lua
--

require 'middleclass'
require 'circle'
require 'vector'
require 'colors'
require 'notifier'
require 'spells/spell_torrent'
require 'spells/spell_tidebringer'
require 'spells/spell_xmarks'
require 'spells/spell_ghostship'
require 'utility'

SpellManager = class('SpellManager')

function SpellManager:initialize(spells)
  self.spritesheet = sprites.ui
  self.iconSize = 105
  self.icon_scale = 0.80
  self.padding = 22
  self.enabled = true

  self.spells = spells

  for index, spell in ipairs(self.spells) do
    spell.icon_scale = self.icon_scale
  end

  self.position = vector((love.graphics.getWidth() / 2) - 354, love.graphics.getHeight() - self.iconSize - 12)

  Notifier:listenForMessage('key_pressed', self)
  Notifier:listenForMessage('world_down', self)
  Notifier:listenForMessage('unit_targeted', self)
end

function SpellManager:receiveMessage(message, data)
  if not self.enabled then
    return
  end

  if message == 'key_pressed' then
    local key = data

    if key == 'escape' then
      for index, spell in ipairs(self.spells) do
        spell:disarm()
      end
    end

    -- Only process the keypress if it's a spell hotkey
    if in_table(key, {'q', 'e', 'r'}) then
      for index, spell in ipairs(self.spells) do
        if key == spell.hotkey then
          spell:arm()
        else
          spell:disarm()
        end
      end
    end
  end

  if message == 'world_down' then
    local worldPoint = data
    for index, spell in ipairs(self.spells) do
      if spell.armed and spell.type == 'target_point' then
        spell:triggerOnPoint(worldPoint)
      end
    end
  end

  if message == 'unit_targeted' then
    local unit = data
    for index, spell in ipairs(self.spells) do
      if spell.armed and spell.type == 'target_unit' then
        spell:triggerOnUnit(unit)
      end
    end
  end
end

function SpellManager:update(dt)
  local armed = false
  for index, spell in ipairs(self.spells) do
    spell:update(dt)
    if spell.armed then
      armed = true
    end
  end
  if armed and self.enabled then
    cursor.quad_name = 'cursor_spell_enemy.png'
  else
    cursor.quad_name = 'cursor_default.png'
  end
end

function SpellManager:drawIcons()
  for index, spell in ipairs(self.spells) do
    local position = self.position + vector(((self.iconSize * self.icon_scale + self.padding) * index) - self.iconSize, 0)
    spell:drawIcon(position)
  end
end

function SpellManager:drawOverlays()
  for index, spell in ipairs(self.spells) do
    local position = self.position + vector(((self.iconSize * self.icon_scale + self.padding) * index) - self.iconSize, 0)
    spell:drawOverlay(position)
  end
end

function SpellManager:drawText()
  for index, spell in ipairs(self.spells) do
    local position = self.position + vector(((self.iconSize * self.icon_scale + self.padding) * index) - self.iconSize, 0)
    spell:drawText(position)
  end
end

function SpellManager:drawEffects()
  for index, spell in ipairs(self.spells) do
    spell:drawEffect()
  end
end



