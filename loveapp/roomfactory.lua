--
--  roomfactory.lua
--  rogue-descent
--
--  Created by Jay Roberts on 2012-03-31.
--  Copyright 2012 GloryFish.org. All rights reserved.
--

require 'middleclass'
require 'rooms/room'
require 'rooms/roomtiled'
require 'rooms/box'

RoomFactory = class('RoomFactory')

function RoomFactory:initialize(seed)
  assert(type(seed) == 'number', 'seed must be a number')
  self.seed = seed
end

function RoomFactory:buildRoom(destination, position, size)
  local seed = self.seed + destination.id
  return Box(self.seed + destination.id, destination, position, size)
end