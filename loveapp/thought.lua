--
--  bar.lua
--

require 'middleclass'
require 'vector'
require 'colors'

Thought = class('Thought')

function Thought:initialize(attachedTo)
  self.spritesheet = sprites.main
  self.position = attachedTo.position
  self.attachedTo = attachedTo
  self.quad = ''
  self.visible = false
  self.scale = vector(1, 1)
end

function Thought:set(name)
  if self.quad ~= name then
    self.quad = name

    timer.add(math.random() * 0.3, function()
      self.visible = true
      self.scale = vector(1, 0)
      tween.start(0.5, self.scale, {y = 1}, 'outElastic')

      timer.add(1.5, function()
        self.visible = false
      end)
    end)
  end
end

function  Thought:getSize()
  local quad = self.spritesheet.quads[self.quad]
  local x, y, w, h = quad:getViewport()
  return vector(w, h)
end

function Thought:update(dt)
  self.position = self.attachedTo.position + vector(0, -35)
end

function Thought:draw()
  if self.visible then
    local size = self:getSize()
    colors.white:set()
    self.spritesheet.batch:add(self.spritesheet.quads[self.quad],
                              math.floor(self.position.x),
                              math.floor(self.position.y),
                              self.rotation,
                              self.scale.x * 0.5,
                              self.scale.y * 0.5,
                              size.x / 2,
                              size.y / 2)
  end
end