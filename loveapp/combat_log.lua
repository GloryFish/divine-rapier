--
--  combat_log.lua
--

require 'middleclass'
require 'circle'
require 'vector'
require 'colors'
require 'notifier'
require 'label'

CombatLog = class('CombatLog')

function CombatLog:initialize()
  self.position = vector(0, 0)
  self.font = love.graphics.newFont('resources/fonts/Ubuntu-B.ttf', 16)
  self.lines = {}
  self.shadowOffset = vector(1, 1)
  self.shadowColor = colors.black
  self.maxAge = 6
  self.fadeDuration = 2
  self.maxLines = 5
end

function CombatLog:update(dt)
end

function CombatLog:addLine(string)
  table.insert(self.lines, {
      components = {
        {
          color = colors.white,
          text = string,
        },
      },
      added = love.timer.getTime(),
      alpha = 1,
    })

  if #self.lines > self.maxLines then
    table.remove(self.lines, 1)
  end
end

function CombatLog:addPurchase(hero, item)
  table.insert(self.lines, {
      components = {
        {
          color = hero.ui_color,
          text = hero.name,
        },
        {
          color = colors.white,
          text = string.format('just purchased %s!', item),
        },
      },
      added = love.timer.getTime(),
      alpha = 1,
    })

  if #self.lines > self.maxLines then
    table.remove(self.lines, 1)
  end
end

function CombatLog:addKill(attacker, victim, gold)
  table.insert(self.lines, {
      components = {
        {
          color = attacker.ui_color,
          text = attacker.name,
        },
        {
          color = colors.white,
          text = 'just pwned',
        },
        {
          color = victim.ui_color,
          text = victim.name,
        },
        {
          color = colors.white,
          text = '\'s head for',
        },
        {
          color = colors.yellow,
          text = tostring(gold),
        },
        {
          color = colors.white,
          text = 'gold!',
        },
      },
      added = love.timer.getTime(),
      alpha = 1,
    })

  if #self.lines > self.maxLines then
    table.remove(self.lines, 1)
  end
end


function CombatLog:update(dt)
  for i, line in ipairs(self.lines) do
    local age = love.timer.getTime() - line.added
    if age > self.maxAge + self.fadeDuration then
      line.deleteme = true
    elseif age > self.maxAge then
      line.alpha = 1 - ((age - self.maxAge) / self.fadeDuration)
    end
  end

  for i = #self.lines, 1, -1 do
    if self.lines[i].deleteme then
      table.remove(self.lines, i)
    end
  end
end

function CombatLog:draw()
  love.graphics.setFont(self.font)

  local lineHeight = self.font:getHeight() + 5
  local lineOffset = self.position.y - lineHeight * #self.lines

  for i, line in ipairs(self.lines) do
    local component_offset = 0
    for j, component in ipairs(line.components) do
      component.color:set()

      local textPosition = vector(self.position.x + component_offset, lineOffset + lineHeight * (i - 1))

      self.shadowColor:alpha(line.alpha * 255):set()
      love.graphics.print(component.text,
                          textPosition.x + self.shadowOffset.x,
                          textPosition.y + self.shadowOffset.y)

      component.color:alpha(line.alpha * 255):set()
      love.graphics.print(component.text,
                          textPosition.x,
                          textPosition.y)

      component_offset = component_offset + self.font:getWidth(component.text) + 5

    end
  end
end
