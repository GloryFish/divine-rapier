--
--  spell_manager.lua
--

require 'middleclass'
require 'circle'
require 'vector'
require 'colors'
require 'notifier'
require 'label'

Hud = class('Hud')

function Hud:initialize()
  self.fonts = {
    default = love.graphics.newFont('resources/fonts/Ubuntu-B.ttf', 18),
    score = love.graphics.newFont('resources/fonts/Ubuntu-B.ttf', 25),
  }
  self.spritesheet = sprites.ui
  self.padding = 20

  self.iconScale = 0.5
  self.iconWidth = 128

  self.radiantColors = {
    Color(37, 107, 224),
    Color(78, 246, 192),
    Color(184, 1, 176),
    Color(233, 234, 89),
    Color(221, 107, 28),
  }
  self.direColors = {
    Color(250, 134, 186),
    Color(156, 182, 90),
    Color(86, 211, 235),
    Color(0, 127, 50),
    Color(147, 103, 15),
  }

  self.radiantIcons = {
    'default.png',
    'default.png',
    'default.png',
    'default.png',
    'default.png',
  }
  self.direIcons = {
    'default.png',
    'default.png',
    'default.png',
    'default.png',
    'default.png',
  }

  self.radiantHeroes = {}
  self.direHeroes = {}

  self.radiantScore = 0
  self.direScore = 0

  self.startTime = love.timer.getTime()

  self.timeLabel = Label(self.fonts.default)
  self.timeLabel.position = vector(love.graphics.getWidth() / 2, 13)
  self.timeLabel.text = '12:43'

  self.goldLabel = Label(self.fonts.default)
  self.goldLabel.color = Color(199, 142, 75)
  self.goldLabel.position = vector(love.graphics.getWidth() / 2 + 525, love.graphics.getHeight() - 180)
  self.goldLabel.anchor = vector(1, 0.5)
  self.goldLabel.text = '0'

  self.radiantScoreLabel = Label(self.fonts.score)
  self.radiantScoreLabel.color = colors.white
  self.radiantScoreLabel.position = vector(love.graphics.getWidth() / 2 - 80, 20)
  self.radiantScoreLabel.anchor = vector(0.5, 0.5)
  self.radiantScoreLabel.text = tostring(self.radiantScore)


  self.direScoreLabel = Label(self.fonts.score)
  self.direScoreLabel.color = colors.white
  self.direScoreLabel.position = vector(love.graphics.getWidth() / 2 + 80, 20)
  self.direScoreLabel.anchor = vector(0.5, 0.5)
  self.direScoreLabel.text = tostring(self.direScore)
end

function Hud:update(dt)
  local elapsed = love.timer.getTime() - self.startTime + 2225
  local hours, minutes = math.floor(elapsed / 60), math.floor(elapsed % 60)
  self.timeLabel.text = string.format('%d:%02d', hours, minutes)
  self.radiantScoreLabel.text = tostring(self.radiantScore)
  self.direScoreLabel.text = tostring(self.direScore)
end

function Hud:setRadiantHeroes(heroes)
  assert(#heroes <= 5, 'Can\'t have more than 5 heroes on dire side')

  self.radiantHeroes = {}

  for i = 1, #heroes do
    self.radiantIcons[i] = heroes[i].ui_icon
    heroes[i].ui_color = self.radiantColors[i]
    table.insert(self.radiantHeroes, heroes[i])
  end
end

function Hud:setDireHeroes(heroes)
  assert(#heroes <= 5, 'Can\'t have more than 5 heroes on dire side')

  self.direHeroes = {}

  for i = 1, #heroes do
    self.direIcons[i] = heroes[i].ui_icon
    heroes[i].ui_color = self.direColors[i]
    table.insert(self.direHeroes, heroes[i])
  end
end

function Hud:draw()
  if vars.showhud then
    colors.white:set()
    self.spritesheet.batch:add(self.spritesheet.quads['kunkka_portrait.png'],
                                love.graphics.getWidth() / 2 - 560,
                                love.graphics.getHeight() - 205,
                                0,
                                1,
                                1,
                                0,
                                0)

    self.spritesheet.batch:add(self.spritesheet.quads['hud_top.png'],
                                love.graphics.getWidth() / 2 - 640,
                                0,
                                0,
                                1,
                                1,
                                0,
                                0)

    self.spritesheet.batch:add(self.spritesheet.quads['hud_bottom.png'],
                                love.graphics.getWidth() / 2 - 640,
                                love.graphics.getHeight() - 235,
                                0,
                                1,
                                1,
                                0,
                                0)

    -- Draw items
    self.spritesheet.batch:add(self.spritesheet.quads['rapier.png'],
                                love.graphics.getWidth() / 2 + 185,
                                love.graphics.getHeight() - 137,
                                0,
                                0.8,
                                0.8,
                                0,
                                0)
    -- Draw radiant icons
    for i = 1, 5 do
      self.spritesheet.batch:add(self.spritesheet.quads[self.radiantIcons[i]],
                              love.graphics.getWidth() / 2 - 450 + ((self.iconWidth + 4) * self.iconScale * (i - 1)),
                              5,
                              0,
                              self.iconScale,
                              self.iconScale,
                              0,
                              0)
    end

    -- Draw dire icons
    for i = 1, 5 do
      self.spritesheet.batch:add(self.spritesheet.quads[self.direIcons[i]],
                              love.graphics.getWidth() / 2 + 120 + ((self.iconWidth + 4) * self.iconScale * (i - 1)),
                              5,
                              0,
                              self.iconScale,
                              self.iconScale,
                              0,
                              0)
    end
  end
end

function Hud:drawText()
  self.timeLabel:draw()
  self.goldLabel:draw()
  self.radiantScoreLabel:draw()
  self.direScoreLabel:draw()
end

