--
--  slash.lua
--

require 'middleclass'
require 'vector'
require 'colors'

local meleeSounds = {
  'follow_thru01.mp3',
  'follow_thru02.mp3',
  'follow_thru03.mp3',
  'ring01.mp3',
  'ring02.mp3',
  'ring03.mp3',
  'ring04.mp3',
  'thump01.mp3',
  'thump02.mp3',
  'thump03.mp3',
  'whoosh01.mp3',
  'whoosh02.mp3',
  'whoosh03.mp3',
}

AttackMelee = class('AttackMelee')

function AttackMelee:initialize(attachedTo, animation)
  self.spritesheet = sprites.main
  self.animation = animation

  self.currentFrameIndex = 1

  self.position = attachedTo.position
  self.offset = self:getCurrentSize() / 2

  self.scale = 1
  self.flip = 1
  self.rotation = math.atan2(attachedTo.direction:unpack())

  self.elapsed = 0

  self.attachedTo = attachedTo
  self.color = color

  self.slashing = false

end

function AttackMelee:getCurrentSize()
  local currentFrame = self.animation.frames[self.currentFrameIndex]
  local quad = self.spritesheet.quads[currentFrame.name]
  local x, y, w, h = quad:getViewport()
  return vector(w, h)
end

function AttackMelee:trigger(target, damage)
  self.slashing = true
  self.elapsed = 0
  target:takeDamage(damage)
  audio:playRandomSound(meleeSounds)
end

function AttackMelee:update(dt)
  if self.slashing then
    self.position = self.attachedTo.position
    self.rotation = math.atan2(self.attachedTo.direction:unpack())

    self.elapsed = self.elapsed + dt

    -- Handle animation
    if #self.animation.frames > 1 then -- More than one frame
      local duration = self.animation.frames[self.currentFrameIndex].duration

      if self.elapsed > duration then -- Switch to next frame
        self.currentFrameIndex = self.currentFrameIndex + 1
        if self.currentFrameIndex > #self.animation.frames then -- Aaaand back around
          self.currentFrameIndex = 1
          self.slashing = false
        end
        self.elapsed = self.elapsed - duration
      end
    end
  end
end

function AttackMelee:draw()
  if self.slashing then
    colors.white:alpha(0.5 * 255):set()
    local currentFrame = self.animation.frames[self.currentFrameIndex]
    self.spritesheet.batch:add(self.spritesheet.quads[currentFrame.name],
                                math.floor(self.position.x),
                                math.floor(self.position.y),
                                self.animation.rotation - self.rotation,
                                self.scale * self.flip,
                                self.scale,
                                self.offset.x,
                                self.offset.y)
  end
end