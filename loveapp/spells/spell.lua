--
--  spell.lua
--

require 'middleclass'
require 'circle'
require 'vector'
require 'colors'

Spell = class('Spell')

function Spell:initialize(owner)
  self.spritesheet = sprites.main
  self.position = vector(0, 0)
  self.type = 'passive' -- passive, target_point, target_unit, instant
  self.owner = owner
end

function Spell:update(dt)
end

function Spell:trigger(target) -- Target might be a position or an entity
end

function Spell:draw(position)
end

function Spell:drawOverlay(position)
end



