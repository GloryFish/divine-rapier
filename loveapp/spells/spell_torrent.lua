--
--  spell_torrent.lua
--

require 'middleclass'
require 'circle'
require 'vector'
require 'colors'
require 'notifier'
require 'label'

local Spell = class('Spell')

function Spell:initialize(owner)
  self.spritesheet_ui = sprites.ui
  self.spritesheet = sprites.main
  self.icon_scale = 1
  self.particles = {
    torrent_pre_splash = particles:add('torrent_pre_splash')
  }
  self.hotkey = 'q'
  self.type = 'target_point' -- passive, target_point, target_unit, instant
  self.owner = owner
  self.armed = false
  self.range = 200
  self.cooldown = 12
  self.stun_range = 30

  self.fonts = {
    cooldown = love.graphics.newFont('resources/fonts/Ubuntu-R.ttf', 28),
  }

  self.cooldownLabel = Label(self.fonts.cooldown)
  self.cooldownLabel.position = vector(0, 0)
  self.cooldownLabel.text = 0
  self.cooldownLabel.annchor = vector(0.5, 0.5)

  -- internal crap
  self.state = 'inactive'
  self.active = false
  self.triggerTime = nil
  self.pre_duration = 2

  self.big_splash_scale = 2.5
  self.big_splash_duration = 2

  self.triggerTime = love.timer.getTime() - self.cooldown
end

function Spell:update(dt)
  if self.active then
    if self.state == 'pre' and love.timer.getTime() - self.triggerTime > self.pre_duration then
      audio:playSound('ability_geyser.mp3')
      self.splashTime = love.timer.getTime()
      self.state = 'splashing'
      Notifier:postMessage('area_effect', {
        name = 'stun_air',
        position = self.position,
        range = self.stun_range,
        duration = 2,
        startTime = love.timer.getTime()
      })
      Notifier:postMessage('area_effect', {
        name = 'damage',
        position = self.position,
        range = self.stun_range,
        amount = 30,
        startTime = love.timer.getTime()
      })
    elseif self.state == 'splashing' and love.timer.getTime() - self.splashTime > self.big_splash_duration then
      self.active = false
    end
  end
end

function Spell:arm()
  -- Check cooldown
  local timeRemaining = self.cooldown - (love.timer.getTime() - self.triggerTime)
  if  timeRemaining > 0 then
    audio:playSound('ability_not_ready.mp3')
    return
  end
  audio:playSound('ability_arm.mp3')
  self.armed = true
end

function Spell:disarm()
  self.armed = false
end

function Spell:triggerOnPoint(point)
  if self.owner.position:dist(point) < self.range then
    self.state = 'pre'
    self.armed = false
    self.active = true
    self.triggerTime = love.timer.getTime()
    self.triggerPoint = point

    audio:playSound('ability_pre_geyser.mp3')
    self.position = point
    self.particles.torrent_pre_splash:splash(point)
  end
end

function Spell:drawIcon(position)
  colors.white:set()
  self.spritesheet_ui.batch:add(self.spritesheet_ui.quads['torrent.png'],
                              math.floor(position.x),
                              math.floor(position.y),
                              0,
                              self.icon_scale,
                              self.icon_scale,
                              0,
                              0)
end

function Spell:drawOverlay(position)
  if self.armed then
    colors.white:set()
    self.spritesheet_ui.batch:add(self.spritesheet_ui.quads['spell_highlight.png'],
                                math.floor(position.x),
                                math.floor(position.y),
                                0,
                                1,
                                1,
                                5,
                                5)
  end
end

function Spell:drawEffect()
  if self.active and self.state == 'splashing' then
    local duration = (love.timer.getTime() - self.triggerTime - self.pre_duration) / self.big_splash_duration
    local scale_y = math.sin(duration * math.pi) * self.big_splash_scale

    colors.white:set()
    self.spritesheet.batch:add(self.spritesheet.quads['Splash_01.png'],
                                math.floor(self.triggerPoint.x),
                                math.floor(self.triggerPoint.y),
                                0,
                                3,
                                scale_y,
                                12,
                                34)
  end
end

function Spell:drawText(position)
  local timeRemaining = self.cooldown - (love.timer.getTime() - self.triggerTime)
  if  timeRemaining > 0 then
    colors.black:alpha(0.5 * 255):set()
    love.graphics.rectangle('fill', position.x, position.y, 85, 85)

    self.cooldownLabel.position = position + vector(40, 40)
    self.cooldownLabel.text = string.format('%d', timeRemaining + 1)
    self.cooldownLabel:draw()
  end
end

SpellTorrent = Spell


