--
--  spell.lua
--

require 'middleclass'
require 'circle'
require 'vector'
require 'colors'

local Spell = class('Spell')

function Spell:initialize(owner)
  self.spritesheet_ui = sprites.ui
  self.spritesheet = sprites.main
  self.icon_scale = 1
  self.hotkey = 'e'
  self.armed = false
  self.type = 'target_unit'
  self.owner = owner
  self.range = 200
  self.cooldown = 16

  self.fonts = {
    cooldown = love.graphics.newFont('resources/fonts/Ubuntu-R.ttf', 28),
  }

  self.cooldownLabel = Label(self.fonts.cooldown)
  self.cooldownLabel.position = vector(0, 0)
  self.cooldownLabel.text = 0
  self.cooldownLabel.annchor = vector(0.5, 0.5)

  -- internal crap
  self.active = false
  self.triggerTime = love.timer.getTime() - self.cooldown
  self.snapbackTime = love.timer.getTime() - self.cooldown
  self.duration = 4
  self.snap_position = vector(0, 0)
  self.dot_interval = 0.3
  self.last_dot = love.timer.getTime()
  self.dot_positions = {}
end

function Spell:update(dt)
  if self.active then
    if love.timer.getTime() - self.triggerTime > self.duration then
      self:snapback()
    end

    -- Add movement dots
    if love.timer.getTime() - self.last_dot > self.dot_interval then
      table.insert(self.dot_positions, self.targetUnit.position)
      self.last_dot = love.timer.getTime()
    end
  end

end

function Spell:arm()
  -- Trigger snapback
  if self.active then
    self:snapback()
  else
    -- Check cooldown
    local timeRemaining = self.cooldown - (love.timer.getTime() - self.triggerTime)
    if timeRemaining > 0 then
      audio:playSound('ability_not_ready.mp3')
      return
    end
    audio:playSound('ability_arm.mp3')
    self.armed = true
  end
end

function Spell:disarm()
  self.armed = false
end

function Spell:triggerOnUnit(unit)
  if self.owner.position:dist(unit.position) < self.range then
    self.armed = false
    self.active = true
    self.targetUnit = unit
    self.snap_position = unit.position
    self.dot_positions = {}
    self.triggerTime = love.timer.getTime()

    audio:playSound('ability_xmark_target.mp3')
    audio:playSound('ability_xmark_movement.mp3')
  end
end

function Spell:snapback()
  self.active = false
  audio:stopSound('ability_xmark_movement.mp3')
  audio:playSound('ability_xmark_snapback.mp3')
  self.targetUnit.position = self.snap_position
  self.snapbackTime = love.timer.getTime()
end

function Spell:drawIcon(position)
  colors.white:set()
  self.spritesheet_ui.batch:add(self.spritesheet_ui.quads['xmarks.png'],
                              math.floor(position.x),
                              math.floor(position.y),
                              0,
                              self.icon_scale,
                              self.icon_scale,
                              0,
                              0)
end

function Spell:drawOverlay(position)
  if self.armed then
    self.spritesheet_ui.batch:add(self.spritesheet_ui.quads['spell_highlight.png'],
                                math.floor(position.x),
                                math.floor(position.y),
                                0,
                                1,
                                1,
                                5,
                                5)
  end
end

function Spell:drawEffect()
  if self.active then
    colors.white:set()
    self.spritesheet.batch:add(self.spritesheet.quads['xmarks_x.png'],
                                math.floor(self.snap_position.x),
                                math.floor(self.snap_position.y),
                                0,
                                0.3,
                                0.3,
                                50,
                                50)

    for index, position in ipairs(self.dot_positions) do
      self.spritesheet.batch:add(self.spritesheet.quads['xmarks_dot.png'],
                            math.floor(position.x),
                            math.floor(position.y),
                            0,
                            0.3,
                            0.3,
                            16,
                            16)
    end
  end
end

function Spell:drawText(position)
  local timeRemaining = self.cooldown - (love.timer.getTime() - self.snapbackTime)
  if timeRemaining > 0 then
    colors.black:alpha(0.5 * 255):set()
    love.graphics.rectangle('fill', position.x, position.y, 85, 85)

    self.cooldownLabel.position = position + vector(40, 40)
    self.cooldownLabel.text = string.format('%d', timeRemaining + 1)
    self.cooldownLabel:draw()
  end
end


SpellXMarks = Spell


