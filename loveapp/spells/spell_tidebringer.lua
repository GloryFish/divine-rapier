--
--  spell.lua
--

require 'middleclass'
require 'circle'
require 'vector'
require 'colors'

local Spell = class('Spell')

function Spell:initialize(owner)
  self.spritesheet_ui = sprites.ui
  self.spritesheet = sprites.main
  self.icon_scale = 1
  self.hotkey = ''
  self.type = 'passive'
end

function Spell:update(dt)
end

function Spell:arm()
end

function Spell:disarm()
end

function Spell:trigger(target) -- Target might be a position or an entity
end

function Spell:drawIcon(position)
  colors.white:set()
  self.spritesheet_ui.batch:add(self.spritesheet_ui.quads['tidebringer.png'],
                              math.floor(position.x),
                              math.floor(position.y),
                              0,
                              self.icon_scale,
                              self.icon_scale,
                              0,
                              0)
end

function Spell:drawOverlay(position)
end

function Spell:drawEffect()
end

function Spell:drawText()
end

SpellTidebringer = Spell


