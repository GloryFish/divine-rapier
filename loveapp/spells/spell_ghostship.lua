--
--  spell_torrent.lua
--

require 'middleclass'
require 'circle'
require 'vector'
require 'colors'
require 'notifier'
require 'label'

local Spell = class('Spell')

function Spell:initialize(owner)
  self.spritesheet_ui = sprites.ui
  self.spritesheet = sprites.main
  self.icon_scale = 1
  self.particles = {
      spray = particles:add('ghostship_spray'),
      splash = particles:add('ghostship_splash')
  }

  self.hotkey = 'r'
  self.type = 'target_point' -- passive, target_point, target_unit, instant
  self.owner = owner
  self.armed = false
  self.range = 200
  self.cooldown = 40
  self.stun_range = 70

  self.scale_x = 1

  self.fonts = {
    cooldown = love.graphics.newFont('resources/fonts/Ubuntu-R.ttf', 28),
  }

  self.cooldownLabel = Label(self.fonts.cooldown)
  self.cooldownLabel.position = vector(0, 0)
  self.cooldownLabel.text = 0
  self.cooldownLabel.annchor = vector(0.5, 0.5)

  -- internal crap
  self.active = false
  self.triggerTime = love.timer.getTime() - self.cooldown

  self.ghostshipImage = love.graphics.newImage('resources/sprites/ghostship.png')
  self.ghostshipXImage = love.graphics.newImage('resources/sprites/ghostship_x.png')
end

function Spell:update(dt)
  if self.active then
    self.particles.spray.position = self.shipPosition
  end
end

function Spell:arm()
  -- Check cooldown
  local timeRemaining = self.cooldown - (love.timer.getTime() - self.triggerTime)
  if  timeRemaining > 0 then
    audio:playSound('ability_not_ready.mp3')
    return
  end
  audio:playSound('ability_arm.mp3')
  self.armed = true
end

function Spell:disarm()
  self.armed = false
end

function Spell:triggerOnPoint(point)
  if self.owner.position:dist(point) < self.range then
    self.armed = false
    self.active = true
    self.triggerTime = love.timer.getTime()

    self.direction = point - self.owner.position
    self.direction:normalize_inplace()
    self.scale_x = 1
    if self.direction.x < 0 then
      self.scale_x = -1
    end
    self.startPosition = self.owner.position + self.direction * -1 * 150
    self.endPosition = self.owner.position + self.direction * 150
    self.shipPosition = self.startPosition:clone()

    audio:playSound('ability_ghost_ship.mp3')
    audio:playSound('ability_ghost_ship_bell.mp3')

    self.particles.spray:setDirection(self.direction)
    self.particles.spray:start()

    tween.start(3.5, self.shipPosition, { x = self.endPosition.x, y = self.endPosition.y }, 'outQuad', function()
      self.active = false
      self.particles.spray:stop()
    end)

    timer.add(2.2, function()
      audio:playSound('ability_ghost_ship_crash.mp3')
      self.particles.splash:trigger(self.shipPosition)
      Notifier:postMessage('area_effect', {
        name = 'stun',
        position = self.shipPosition,
        range = self.stun_range,
        duration = 2,
        startTime = love.timer.getTime()
      })
      Notifier:postMessage('area_effect', {
        name = 'damage',
        position = self.shipPosition,
        range = self.stun_range,
        amount = 100,
        startTime = love.timer.getTime()
      })
    end)
  end
end

function Spell:drawIcon(position)
  colors.white:set()
  self.spritesheet_ui.batch:add(self.spritesheet_ui.quads['ghostship.png'],
                              math.floor(position.x),
                              math.floor(position.y),
                              0,
                              self.icon_scale,
                              self.icon_scale,
                              0,
                              0)
end

function Spell:drawOverlay(position)
  if self.armed then
    colors.white:set()
    self.spritesheet_ui.batch:add(self.spritesheet_ui.quads['spell_highlight.png'],
                                math.floor(position.x),
                                math.floor(position.y),
                                0,
                                1,
                                1,
                                5,
                                5)
  end
end

function Spell:drawEffect()
  if self.active then
    colors.white:set()

    love.graphics.draw(self.ghostshipXImage,
                                math.floor(self.endPosition.x),
                                math.floor(self.endPosition.y),
                                0,
                                0.5,
                                0.5,
                                50,
                                50)

    love.graphics.setBlendMode('add')

    love.graphics.draw(self.ghostshipImage,
                                math.floor(self.shipPosition.x),
                                math.floor(self.shipPosition.y),
                                0,
                                0.3 * self.scale_x,
                                0.3,
                                410,
                                320)

    love.graphics.setBlendMode('alpha')
  end
end

function Spell:drawText(position)
  local timeRemaining = self.cooldown - (love.timer.getTime() - self.triggerTime)
  if  timeRemaining > 0 then
    colors.black:alpha(0.5 * 255):set()
    love.graphics.rectangle('fill', position.x, position.y, 85, 85)

    self.cooldownLabel.position = position + vector(40, 40)
    self.cooldownLabel.text = string.format('%d', timeRemaining + 1)
    self.cooldownLabel:draw()
  end
end

SpellGhostShip = Spell


