require 'middleclass'
require 'rectangle'
require 'colors'
require 'selection'

Choice = class('Choice')

local properties = {
  'life',
  'money',
  'happiness'
}


function Choice.randomPair(randomizer, level, posA, posB)
  local isGood = randomizer:random() > 0.5

  local properties = random_sample(randomizer, properties, 1)

  return {
    Choice(randomizer, level, posA, properties[1], isGood),
    Choice(randomizer, level, posB, properties[2], isGood)
  }
end

function Choice:initialize(randomizer, level, position, property, isGood)
  self.rect = Rectangle(position, vector(100, 100))
  self.color = Color:random(randomizer)

  self.type = 'instant'
  self.property = property
  self.effect = 'decrease'
  if isGood then
    self.effect = 'increase'
  end
  self.amount = randomizer:random(10)
end

function Choice:__tostring()
  return string.format('Choice: %s', tostring('NA'))
end

function Choice:apply(object)
  local adjustment = self.amount
  if self.effect == 'decrease' then
    adjustment = self.amount * -1
  end

  object[self.property] = object[self.property] + adjustment
end


function Choice:draw()
  colors.black:set()
  self.rect:draw(vector(0, 0), 'fill')

  self.color:set()
  self.rect:draw()

  colors.white:set()

  love.graphics.print(self.effect, self.rect.position.x, self.rect.position.y)
  love.graphics.print(self.property, self.rect.position.x, self.rect.position.y + 20)
  love.graphics.print('by', self.rect.position.x, self.rect.position.y + 40)
  love.graphics.print(tostring(self.amount), self.rect.position.x, self.rect.position.y + 60)
end