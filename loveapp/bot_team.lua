--
--  team.lua
--

require 'middleclass'
require 'vector'
require 'states/state_idle'
require 'states/state_bot_dying'
require 'states/state_bot_dead'
require 'states/state_bot_fleeing'
require 'states/state_bot_move_to_enemy'
require 'states/state_bot_seek_enemy'
require 'states/state_bot_attack'
require 'utility'

BotTeam = class('BotTeam')

function BotTeam:initialize()
  self.players = {}

  self.enemy_last_position = nil
  self.enemy_last_seen_time = 0

  self.patrols = {
    { -- Dire jungle
      vector(125, 187),
      vector(455, 220),
      vector(503, 432),
      vector(155, 127),
      vector(550, 150),
    },
    { -- Mid
      vector(490, 510),
      vector(330, 650),
      vector(545, 667),
      vector(245, 450),
      vector(708, 310),
    },
    { -- Bottom
      vector(910, 394),
      vector(770, 600),
      vector(616, 784),
      vector(441, 897),
      vector(870, 868),
    },
    { -- River
      vector(106, 340),
      vector(330, 350),
      vector(470, 525),
      vector(740, 670),
      vector(930, 680),
      vector(740, 670),
      vector(470, 525),
      vector(330, 350),
    },
    { -- Aggressive
      vector(100, 650),
      vector(280, 715),
      vector(350, 890),
      vector(150, 830),
    },
  }

  self.state = nil
end

function BotTeam:setState(state)
  if (self.state ~= state) then
    self.state = state

    if self.state == 'team_hunt_enemy' then -- Move players into patrol positions
      for index, player in ipairs(self.players) do
        if index <= #self.patrols then
          if not in_table(player.state.name, {'state_bot_fleeing', 'state_bot_dying', 'state_bot_dead'}) then
            player:setState(StateBotSeekEnemy())
            player.patrol = self.patrols[index]
            player.currentPatrolIndex = self:nearestPatrolPointIndex(player.position, player.patrol)
          end
        end
      end
    end

    if self.state == 'team_attack_enemy' then -- Move players towards enemy and attack if able
      for index, player in ipairs(self.players) do
        if not in_table(player.state.name, {'state_bot_fleeing', 'state_bot_dying', 'state_bot_dead'}) then
          player:setState(StateBotMoveToEnemy())
        end
      end
    end
  end
end

function BotTeam:nearestPatrolPointIndex(position, patrol)
  local min_dist = math.huge
  local min_index = 1
  for index, point in ipairs(patrol) do
    local dist = point:dist(position)
    if dist < min_dist then
      min_dist = dist
      min_index = index
    end
  end
  return min_index
end

function BotTeam:addPlayer(player)
  player.team = self
  table.insert(self.players, player)
end

function BotTeam:sawEnemyAt(position)
  self.enemy_last_position = position
  self.enemy_last_seen_time = 0

  for index, player in ipairs(self.players) do
    player.enemy_last_position = position
    player.enemy_last_seen_time = love.timer.getTime()
  end

  self:setState('team_attack_enemy')
end

function BotTeam:updateEnemyPosition(position)
  for index, player in ipairs(self.players) do
    if player.state.name == 'state_bot_move_to_enemy' then
      player.enemy_last_position = position
      player.enemy_last_seen_time = love.timer.getTime()
    end
  end
end

function BotTeam:update(dt)
  if self.state == 'team_attack_enemy' then
    local engaging = false
    for index, player in ipairs(self.players) do
      if player.state.name == 'state_bot_move_to_enemy' or player.state.name == 'state_bot_attack' then
        engaging = true
        break
      end
    end
    if not engaging then
      self:setState('team_hunt_enemy')
    end
  end
end