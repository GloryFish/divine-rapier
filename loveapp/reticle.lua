--
--  circle.lua
--

require 'middleclass'
require 'circle'
require 'vector'
require 'colors'

Reticle = class('Reticle')
function Reticle:initialize()
  self.spritesheet = sprites.main
  self.position = vector(0, 0)
  self.offset = vector(16, 32)

  self.rotation = 0
  self.show_start = 0
  self.show_duration = 0.3
  self.showing = false
  self.scale = 0.3
  self.max_spread = 15
  self.spread = self.max_spread
end

function Reticle:show(position)
  self.show_start = love.timer.getTime()
  self.position = position
  self.duration = 0
  self.showing = true
end

function Reticle:update(dt)
  local duration = love.timer.getTime() - self.show_start
  if duration > self.show_duration then
    self.showing = false
  else
    self.spread = (1 - duration / self.show_duration) * self.max_spread
  end
end

function Reticle:draw()
  if self.showing then
    colors.white:set()
    -- left
    self.spritesheet.batch:add(self.spritesheet.quads['movement-arrow.png'],
                                math.floor(self.position.x) - self.spread,
                                math.floor(self.position.y),
                                self.rotation - math.pi / 2,
                                self.scale,
                                self.scale,
                                self.offset.x,
                                self.offset.y)

    -- right
    self.spritesheet.batch:add(self.spritesheet.quads['movement-arrow.png'],
                                math.floor(self.position.x) + self.spread,
                                math.floor(self.position.y),
                                self.rotation + math.pi / 2,
                                self.scale,
                                self.scale,
                                self.offset.x,
                                self.offset.y)

    -- right
    self.spritesheet.batch:add(self.spritesheet.quads['movement-arrow.png'],
                                math.floor(self.position.x),
                                math.floor(self.position.y) + self.spread,
                                self.rotation + math.pi,
                                self.scale,
                                self.scale,
                                self.offset.x,
                                self.offset.y)

    -- right
    self.spritesheet.batch:add(self.spritesheet.quads['movement-arrow.png'],
                                math.floor(self.position.x),
                                math.floor(self.position.y) - self.spread,
                                self.rotation,
                                self.scale,
                                self.scale,
                                self.offset.x,
                                self.offset.y)

  end
end




